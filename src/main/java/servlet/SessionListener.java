package servlet;

import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * User: vx(home)
 * Date: 3/13/14
 * Time: 9:49 PM
 */
public class SessionListener implements HttpSessionListener {
    private static Logger log = Logger.getLogger(SessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        final ServletContext context = httpSessionEvent.getSession().getServletContext();
        synchronized (SessionListener.class) {
            Integer clientsConnected = (Integer) context.getAttribute("clientsConnected");
            if (clientsConnected == null) {
                clientsConnected = 1;
            } else {
                clientsConnected++;
            }
            log.info("New session created. clientsConnected=" + clientsConnected);
            context.setAttribute("clientsConnected", clientsConnected);
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        final ServletContext context = httpSessionEvent.getSession().getServletContext();
        synchronized (SessionListener.class) {
            Integer clientsConnected = (Integer) context.getAttribute("clientsConnected");
            clientsConnected--;
            log.info("Session destroyed. clientsConnected=" + clientsConnected);
            context.setAttribute("clientsConnected", clientsConnected);
        }
    }
}
