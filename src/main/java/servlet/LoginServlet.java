package servlet;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: vx(home)
 * Date: 3/13/14
 * Time: 8:05 PM
 */
public class LoginServlet extends HttpServlet {
    static Logger log = Logger.getLogger(LoginServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String clientName = req.getParameter("clientName");
        if (clientName == null) {
            log.error("Client not found");
            throw new ServletException("No client specified.");
        }
        req.getSession().setAttribute("clientName", clientName);
        log.info("Client " + clientName + " has logged into web ATM");

        resp.sendRedirect("/BankApp/menu.html");
    }
}
