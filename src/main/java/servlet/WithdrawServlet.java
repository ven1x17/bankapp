package servlet;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.service.bank.PersistenceService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * User: vx(home)
 * Date: 3/13/14
 * Time: 10:25 PM
 */
public class WithdrawServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(WithdrawServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String clientName = (String) req.getSession().getAttribute("clientName");
        Float amount = Float.valueOf(req.getParameter("amount"));
        PersistenceService ps = null;

        try {
            ps = PersistenceService.getInstance();
            Client c = ps.findClientByName(clientName);

            if (c != null) {
                c.withdraw(amount);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NotEnoughFundsException e) {
            // catch exception
        }

        resp.sendRedirect("/BankApp/balance");
    }
}
