package servlet;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.service.bank.PersistenceService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * User: vx(home)
 * Date: 3/13/14
 * Time: 10:25 PM
 */
public class BalanceServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(BalanceServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String clientName = (String) req.getSession().getAttribute("clientName");
            PersistenceService ps = PersistenceService.getInstance();
            Client c = ps.findClientByName(clientName);
//            ServletOutputStream out = resp.getOutputStream();

            if (c == null) {
//                out.println("Wrong client name");
                List<Client> list = ps.getAllClients();
                StringBuilder names = new StringBuilder();
                for (Client client : list) {
                    names.append(client.getName() + ", ");
                }
//                out.println("Client list: " + names.toString());
                req.setAttribute("clientName", "Wrong client name<br>Client list: " + names.toString());
            } else {
//                out.println(clientName + " has account balance = " + c.getBalance());
                req.setAttribute("clientName", clientName);
                req.setAttribute("balance", c.getBalance());
            }

            req.getRequestDispatcher("/balance.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
