package servlet;

import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: vx(home)
 * Date: 3/13/14
 * Time: 9:56 PM
 */
public class SessionAmountServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(SessionAmountServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final ServletContext context = req.getSession().getServletContext();
        Integer clientsConnected = (Integer) context.getAttribute("clientsConnected");

        ServletOutputStream out = resp.getOutputStream();
        out.println("Currently connected sessions: " + clientsConnected);
    }
}
