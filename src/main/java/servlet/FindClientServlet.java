package servlet;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.service.bank.PersistenceService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by vx on 14.03.14 .
 * Time: 14:35
 */
public class FindClientServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(FindClientServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/findClient.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            PersistenceService ps = PersistenceService.getInstance();
            final String name = req.getParameter("name");
            final String city = req.getParameter("city");

//            log.info("Search result for client " + name + ":" + client);
            List<Client> result = ps.findClientByNameAndCity(name, city);

            req.setAttribute("clients", result);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        req.getRequestDispatcher("/findClient.jsp").forward(req, resp);
    }
}
