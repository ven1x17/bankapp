package servlet;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.service.bank.PersistenceService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by vx on 14.03.14 .
 * Time: 16:51
 */
public class SaveClientServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(SaveClientServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("clientId");
        log.info("Save request for client id=" + id);
        if (id == null) {
            id = "0";
        }
        try {
            PersistenceService ps = PersistenceService.getInstance();
            Client c = ps.getClientById(Integer.parseInt(id));
            c.setName(req.getParameter("name"));
            c.setCity(req.getParameter("city"));
            c.setEmail(req.getParameter("email"));
            c.setPhone(req.getParameter("phone"));
            ps.saveClient(c);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        resp.sendRedirect("/BankApp/client?clientId=" + id);
    }
}
