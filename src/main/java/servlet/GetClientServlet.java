package servlet;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.service.bank.PersistenceService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by vx on 14.03.14 .
 * Time: 16:37
 */
public class GetClientServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(GetClientServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("clientId");
        Client result = null;
        try {
            result = PersistenceService.getInstance().getClientById(Integer.parseInt(id));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        req.setAttribute("client", result);
        req.getRequestDispatcher("/clientInfo.jsp").forward(req, resp);
    }
}
