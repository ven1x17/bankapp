package servlet;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: vx(home)
 * Date: 3/13/14
 * Time: 7:42 PM
 */
public class IndexServlet extends HttpServlet {
    private static Logger log = Logger.getLogger(SessionAmountServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletOutputStream out = resp.getOutputStream();
        out.println("Welcome to web ATM! <br>");
        out.println("<a href='login.html'>Login page</a>");
    }
}
