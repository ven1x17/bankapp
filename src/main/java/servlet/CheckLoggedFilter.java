package servlet;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * User: vx(home)
 * Date: 3/13/14
 * Time: 9:25 PM
 */
public class CheckLoggedFilter implements Filter {
    private static Logger log = Logger.getLogger(SessionAmountServlet.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) servletRequest).getSession();
        String clientName = (String) session.getAttribute("clientName");
        String path = ((HttpServletRequest) servletRequest).getRequestURI();
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        if (path.startsWith("/BankApp/login") ||
                path.startsWith("/resources") ||
                "/BankApp/bankapp".equals(path) ||
                "/BankApp/".equals(path) ||
                clientName != null) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            resp.sendRedirect("/BankApp/login.html");
        }
    }

    @Override
    public void destroy() {

    }
}
