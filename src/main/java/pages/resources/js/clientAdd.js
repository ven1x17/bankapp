function validateClientAdd(){
var name = $("#name").val();
var city = $("#city").val();
//var genderf = $("#genderf").val();
//var genderm = $("#genderm").val();
var email = $("#email").val();
var openAcc = $("#openAcc").val();
var startingBalance = $("#startingBalance").val();

var errors = "";

if (name.length < 1){
    errors += "<tr><td>ФИО не заполнено<td></tr>"
} else {
    var nameArr = name.split(" ");
    if (nameArr.length != 3){
        errors += "<tr><td>ФИО написано неправильно<td></tr>"
    }
    for(var i=0;i<nameArr.length;i++) {
          var n = nameArr[i];
         if (n.length < 2 || n.length > 15){
            errors += "<tr><td>Часть ФИО слишком короткая или слишком длинная<td></tr>"
         }
    }
}
if (city.length < 1){
    errors += "<tr><td>city не заполнено<td></tr>"
}
if ($('input[name=gender]:checked').length < 1){
    errors += "<tr><td>gender не заполнено<td></tr>"
}
if (email.length < 1){
    errors += "<tr><td>email не заполнено<td></tr>"
} else {
    if (!email.match(".+@.+\.[a-zA-Z]{2,3}")) {
            errors += "<tr><td>email имеет неправильный формат<td></tr>"
    }
}
if (startingBalance.length < 1){
    errors += "<tr><td>startingbalance не заполнено<td></tr>"
}
if (errors != ""){
$("#errors").html(errors);
} else {
$("#errors").html("<tr><td>Успех<td></tr>");
}
return false;
}