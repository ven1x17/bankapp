  function withdraw() {
        var form = $("#withdrawForm"); // find form by id
        var checkResult = true;
        var sum=$("#sum").val();
        if (!isNumeric(sum)) {
                            $("#sumError").html("Неправильный формат");
                            checkResult = false;
                        }
        if (sum <= 0) {
                            $("#sumError").html("Сумма вывода должна быть положительной");
                            checkResult = false;
                        }
        if (checkResult) {
            return true;
        } else {
            return false;
        }
    }

    function isNumeric(input){
        var RE = /^-{0,1}\d*\.{0,1}\d+$/;
        return (RE.test(input));
    }