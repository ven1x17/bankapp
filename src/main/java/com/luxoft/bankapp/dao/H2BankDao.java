package com.luxoft.bankapp.dao;

import com.luxoft.bankapp.domain.bank.*;
import com.luxoft.bankapp.service.bank.PersistenceService;
import org.h2.jdbc.JdbcSQLException;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: vx
 * Date: 2/22/14
 * Time: 7:39 PM
 */
@Repository
public class H2BankDao extends BaseDao implements BankDao {

    private Connection conn;

    public H2BankDao() {
    }

    @Override
    public Bank getBankById(int id) throws SQLException {
        conn = openConnection();
        try {
            try {
                Statement st = conn.createStatement();
                String sql = "SELECT bankId, address FROM banks WHERE bankId = " + id;
                ResultSet rs = st.executeQuery(sql);
                if (rs.next() == false) {
                    return null;
                }
                Bank result = new Bank(rs.getInt("bankId"), rs.getString("address"), new ArrayList<ClientRegistrationListener>());
                return result;
            } catch (JdbcSQLException ex) {
                return null;
            }
        } finally {
            closeConnection();
        }
    }

    @Override
    public List<Bank> getAllBanks() throws SQLException {
        conn = openConnection();
        try {
            ArrayList<Bank> result = new ArrayList<Bank>();

            Statement st = conn.createStatement();
            String sql = "SELECT bankId, address FROM banks ";
            ResultSet rs = st.executeQuery(sql);
            while (rs.next() != false) {
                result.add(new Bank(rs.getInt("bankId"), rs.getString("address"), new ArrayList<ClientRegistrationListener>()));
            }
            return result;
        } finally {
            closeConnection();
        }
    }

    @Override
    public BankInfo getBankInfo() throws SQLException, ClassNotFoundException {
        conn = openConnection();
        try {
            BankInfo binfo = new BankInfo();
            List<Client> list = PersistenceService.getInstance().getAllClients();
            binfo.setNumberOfClients(list.size());
            double sum = 0;
            Map<String, List<Client>> citymap = new HashMap<>();
            for (Client c : list) {

                if (citymap.containsKey(c.getCity())) {
                    citymap.get(c.getCity()).add(c);
                } else {
                    ArrayList<Client> cl = new ArrayList<Client>();
                    cl.add(c);
                    citymap.put(c.getCity(), cl);
                }

                for (Account acc : c.getAccounts()) {
                    sum += acc.getBalance();
                }
            }
            binfo.setTotalAccountSum(sum);
            binfo.setClientsByCity(citymap);
            return binfo;
        } finally {
            closeConnection();
        }
    }
}
