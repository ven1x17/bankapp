package com.luxoft.bankapp.dao;

import com.luxoft.bankapp.domain.bank.Client;

import java.sql.SQLException;
import java.util.List;

/**
 * User: vx
 * Date: 2/22/14
 * Time: 5:59 PM
 */
public interface ClientDao {

    public void save(Client client) throws SQLException;

    void remove(Client client) throws SQLException;

    public Client getClientById(int id) throws SQLException;

    public Client getClientByName(String name) throws SQLException;

    List<Client> getClientByNameAndCity(String name, String city) throws SQLException;

    public List<Client> getAllClients() throws SQLException;
}

