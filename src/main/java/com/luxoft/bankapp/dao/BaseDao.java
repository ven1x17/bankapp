package com.luxoft.bankapp.dao;

import com.luxoft.bankapp.service.bank.PersistenceService;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * User: vx(home)
 * Date: 2/24/14
 * Time: 8:26 PM
 */
public class BaseDao {

    Connection conn;

    public Connection openConnection() throws SQLException {
        try {
            File f = new File("bankapp.h2.db");
            if (!f.exists()) {
                PersistenceService.getInstance().initDatabase();
            }
            Class.forName("org.h2.Driver");
            conn = DriverManager.getConnection("jdbc:h2:bankapp", "sa", "");
            return conn;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void closeConnection() throws SQLException {
        conn.close();
    }
}
