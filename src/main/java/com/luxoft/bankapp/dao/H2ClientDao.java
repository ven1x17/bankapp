package com.luxoft.bankapp.dao;

import com.luxoft.bankapp.domain.bank.AbstractAccount;
import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.Gender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: vx
 * Date: 2/22/14
 * Time: 7:38 PM
 */
@Repository
public class H2ClientDao extends BaseDao implements ClientDao {
    @Autowired
    AccountDao accountDao;

    private Connection conn;

    public H2ClientDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public H2ClientDao() {
    }

    public AccountDao getAccountDao() {
        return accountDao;
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void save(Client client) throws SQLException {
        Client c = null;
        try {
            c = getClientByName(client.getName());

            conn = openConnection();
            if (c == null) {
                final String sql = "INSERT INTO clients(name, gender, email, phone, bankId) VALUES(?, ?, ?, ?, ?)";
                final PreparedStatement st = conn.prepareStatement(sql);

                st.setString(1, client.getName());
                st.setString(2, client.getGender().toString());
                st.setString(3, client.getEmail());
                st.setString(4, client.getPhone());
                st.setInt(5, client.getBankId());
                st.execute();
                closeConnection();

                int id = getClientByName(client.getName()).getId();
                for (Account a : client.getAccounts()) {
                    ((AbstractAccount) a).setClientId(id);
                    accountDao.save(a);
                }
            } else {
                final String sql = "UPDATE clients SET gender=?, email=?, phone=?, bankId=? city=? WHERE name = '" + client.getName() + "'";
                final PreparedStatement st = conn.prepareStatement(sql);

                st.setString(1, client.getGender().toString());
                st.setString(2, client.getEmail());
                st.setString(3, client.getPhone());
                st.setInt(4, client.getBankId());
                st.setString(5, client.getCity());
                st.execute();

                int id = getClientByName(client.getName()).getId();
                for (Account a : client.getAccounts()) {
                    ((AbstractAccount) a).setClientId(id);
                    accountDao.save(a);
                }
            }
        } finally {
            closeConnection();
        }
    }

    @Override
    public void remove(Client client) throws SQLException {
        try {
            conn = openConnection();
            Statement st = conn.createStatement();
            String sql = "DELETE FROM clients WHERE clientId = '" + client.getId() + "'";
            st.executeQuery(sql);
            accountDao.removeForClientId(client.getId());
        } finally {
            closeConnection();
        }
    }

    @Override
    public Client getClientById(int id) throws SQLException {
        try {
            conn = openConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM clients WHERE clientId = '" + id + "'";
            ResultSet rs = st.executeQuery(sql);
            if (rs.next() == false) {
                return null;
            }
            Client result = new Client(rs.getInt("clientId"), rs.getInt("bankId"), rs.getString("name"), Gender.getGender(rs.getString("gender")), rs.getString("email"), rs.getString("phone"));
            result.setCity(rs.getString("city"));
            List<Account> list = accountDao.getAllForClient(id);
            for (Account acc : list) {
                result.addAccount(acc);
            }
            return result;
        } finally {
            closeConnection();
        }
    }

    @Override
    public Client getClientByName(String name) throws SQLException {
        try {
            conn = openConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM clients WHERE name = '" + name + "'";
            ResultSet rs = st.executeQuery(sql);
            if (rs.next() == false) {
                return null;
            }
            Client result = new Client(rs.getInt("clientId"), rs.getInt("bankId"), rs.getString("name"), Gender.getGender(rs.getString("gender")), rs.getString("email"), rs.getString("phone"));
            result.setCity(rs.getString("city"));
            List<Account> list = accountDao.getAllForClient(result.getId());
            for (Account acc : list) {
                result.addAccount(acc);
            }
            return result;
        } finally {
            closeConnection();
        }
    }

    @Override
    public List<Client> getClientByNameAndCity(String name, String city) throws SQLException {
        try {
            conn = openConnection();
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM clients WHERE name LIKE '%" + name + "%' AND city LIKE '%" + city + "%'";
            ResultSet rs = st.executeQuery(sql);

            List<Client> result = new ArrayList<>();
            while (rs.next() != false) {
                Client client = new Client(rs.getInt("clientId"), rs.getInt("bankId"), rs.getString("name"), Gender.getGender(rs.getString("gender")), rs.getString("email"), rs.getString("phone"));

                client.setCity(rs.getString("city"));
                List<Account> list = accountDao.getAllForClient(client.getId());
                for (Account acc : list) {
                    client.addAccount(acc);
                }
                result.add(client);
            }

            return result;
        } finally {
            closeConnection();
        }
    }

    @Override
    public List<Client> getAllClients() throws SQLException {
        try {
            conn = openConnection();
            ArrayList<Client> result = new ArrayList<Client>();

            Statement st = conn.createStatement();
            String sql = "SELECT * FROM clients ";
            ResultSet rs = st.executeQuery(sql);
            while (rs.next() != false) {
                Client client = new Client(rs.getInt("clientId"), rs.getInt("bankId"), rs.getString("name"), Gender.getGender(rs.getString("gender")), rs.getString("email"), rs.getString("phone"));
                client.setCity(rs.getString("city"));
                List<Account> list = accountDao.getAllForClient(client.getId());
                for (Account acc : list) {
                    client.addAccount(acc);
                }
                result.add(client);
            }
            return result;
        } finally {
            closeConnection();
        }
    }
}
