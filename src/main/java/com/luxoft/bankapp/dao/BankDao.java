package com.luxoft.bankapp.dao;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.BankInfo;

import java.sql.SQLException;
import java.util.List;

/**
 * User: vx
 * Date: 2/22/14
 * Time: 5:57 PM
 */
public interface BankDao {
    public Bank getBankById(int id) throws SQLException;

    public List<Bank> getAllBanks() throws SQLException;

    public BankInfo getBankInfo() throws SQLException, ClassNotFoundException;
}
