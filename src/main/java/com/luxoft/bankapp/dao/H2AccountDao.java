package com.luxoft.bankapp.dao;

import com.luxoft.bankapp.domain.bank.AbstractAccount;
import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.CheckingAccount;
import com.luxoft.bankapp.domain.bank.SavingAccount;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: vx(home)
 * Date: 2/23/14
 * Time: 8:22 PM
 */
@Repository
public class H2AccountDao extends BaseDao implements AccountDao {

    private Connection conn;

    public H2AccountDao() {
    }

    @Override
    public void save(Account acc) throws SQLException {
        try {
            Account a = null;
            a = getById(((AbstractAccount) acc).getId());

            conn = openConnection();
            if (a == null) {
                if (acc instanceof CheckingAccount) {
                    final String sql = "INSERT INTO accounts(type, balance, overdraft, max_overdraft, clientId) VALUES(?, ?, ?, ?, ?)";
                    final PreparedStatement st = conn.prepareStatement(sql);

                    st.setString(1, "checking");
                    st.setFloat(2, acc.getBalance());
                    st.setFloat(3, ((CheckingAccount) acc).getOverdraft());
                    st.setFloat(4, ((CheckingAccount) acc).getMaxOverdraft());
                    st.setInt(5, ((CheckingAccount) acc).getClientId());
                    st.execute();
                } else if (acc instanceof SavingAccount) {
                    final String sql = "INSERT INTO accounts(type, balance, clientId) VALUES(?, ?, ?)";
                    final PreparedStatement st = conn.prepareStatement(sql);

                    st.setString(1, "saving");
                    st.setFloat(2, acc.getBalance());
                    st.setInt(3, ((SavingAccount) acc).getClientId());
                    st.execute();
                }
            } else {
                if (acc instanceof CheckingAccount) {
                    final String sql = "UPDATE accounts SET balance='" + acc.getBalance() + "', overdraft='" + ((CheckingAccount) acc).getOverdraft() + "', max_overdraft='" + ((CheckingAccount) acc).getMaxOverdraft() + "' WHERE accountId = '" + ((AbstractAccount) acc).getId() + "'";
                    Statement st = conn.createStatement();
                    st.execute(sql);
                } else if (acc instanceof SavingAccount) {
                    final String sql = "UPDATE accounts SET balance=? WHERE accountId = '" + ((AbstractAccount) acc).getId() + "'";
                    final PreparedStatement st = conn.prepareStatement(sql);

                    st.setFloat(1, acc.getBalance());
                    st.execute();
                }
            }
        } finally {
            closeConnection();
        }
    }

    @Override
    public void removeForClientId(int id) throws SQLException {
        conn = openConnection();
        try {
            Statement st = conn.createStatement();
            String sql = "DELETE FROM accounts WHERE clientId = '" + id + "'";
            st.executeQuery(sql);
        } finally {
            closeConnection();
        }
    }

    @Override
    public Account getById(int id) throws SQLException {
        conn = openConnection();
        try {
            Statement st = conn.createStatement();
            String sql = "SELECT * FROM accounts WHERE accountId = '" + id + "'";
            ResultSet rs = st.executeQuery(sql);
            if (rs.next() == false) {
                return null;
            }
            Account result = null;
            String type = rs.getString("type");
            switch (type) {
                case "saving":
                    result = new SavingAccount(rs.getInt("accountId"), rs.getInt("clientId"), rs.getFloat("balance"));
                    break;
                case "checking":
                    result = new CheckingAccount(rs.getInt("accountId"), rs.getInt("clientId"), rs.getFloat("balance"), rs.getFloat("overdraft"), rs.getFloat("max_overdraft"));
                    break;
            }
            return result;
        } finally {
            closeConnection();
        }
    }

    @Override
    public List<Account> getAllForClient(int id) throws SQLException {
        conn = openConnection();
        try {
            ArrayList<Account> result = new ArrayList<Account>();

            Statement st = conn.createStatement();
            String sql = "SELECT * FROM accounts WHERE clientId='" + id + "'";
            ResultSet rs = st.executeQuery(sql);
            while (rs.next() != false) {
                String type = rs.getString("type");
                switch (type) {
                    case "saving":
                        result.add(new SavingAccount(rs.getInt("accountId"), rs.getInt("clientId"), rs.getFloat("balance")));
                        break;
                    case "checking":
                        result.add(new CheckingAccount(rs.getInt("accountId"), rs.getInt("clientId"), rs.getFloat("balance"), rs.getFloat("overdraft"), rs.getFloat("max_overdraft")));
                        break;
                }
            }
            return result;
        } finally {
            closeConnection();
        }
    }
}
