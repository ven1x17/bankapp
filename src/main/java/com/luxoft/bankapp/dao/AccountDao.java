package com.luxoft.bankapp.dao;

import com.luxoft.bankapp.domain.bank.Account;

import java.sql.SQLException;
import java.util.List;

/**
 * User: vx
 * Date: 2/22/14
 * Time: 5:59 PM
 */
public interface AccountDao {
    public void save(Account client) throws SQLException;

    public void removeForClientId(int id) throws SQLException;

    public Account getById(int id) throws SQLException;

    public List<Account> getAllForClient(int id) throws SQLException;
}
