package com.luxoft.bankapp.socket.v2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * User: vx(home)
 * Date: 3/5/14
 * Time: 9:11 PM
 */
@Deprecated
public class WebClient {
    private Socket conn;
    private OutputStream out;
    private InputStream in;

    public WebClient(Socket conn) {
        this.conn = conn;
    }

    public String getResponse() throws IOException {
        // Getting Input and Output streams
        out = conn.getOutputStream();
        in = conn.getInputStream();

        return convertStreamToString(in);
    }

    private static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
