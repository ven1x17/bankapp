package com.luxoft.bankapp.socket.v2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * Created by vx on 05.03.14 .
 * Time: 16:36
 */
public class BankServerThreaded {
    public static final int PORT = 8080;
    private int poolSize = 50;
    private final ServerSocket serverSocket;
    private final ExecutorService pool;

    private volatile boolean shouldStop = false;
    private AtomicInteger connectionCount = new AtomicInteger(0);
    private static BankServerThreaded instance;
    private static Logger logger = Logger.getLogger(BankServerThreaded.class.getName());

    public BankServerThreaded() throws IOException {
        serverSocket = new ServerSocket(PORT);
        pool = Executors.newFixedThreadPool(poolSize);
        instance = this;
    }

    public void startThreads() throws IOException {
        try {
            while (!shouldStop) {
                Socket clientSocket = serverSocket.accept();
                pool.execute(new ServerThread(clientSocket));

            }
        } finally {
            serverSocket.close();
        }
    }


    public static BankServerThreaded getInstance() throws IOException {
        if (instance == null) {
            return new BankServerThreaded();
        } else
            return instance;
    }

    public void incrementConnectionCount() {
        connectionCount.getAndIncrement();
    }

    public void decrementConnectionCount() {
        connectionCount.getAndDecrement();
    }

    public int getConnectionCount() {
        return connectionCount.get();
    }

    public void stop() {
        shouldStop = true;
    }
}
