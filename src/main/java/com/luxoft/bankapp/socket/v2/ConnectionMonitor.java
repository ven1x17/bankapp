package com.luxoft.bankapp.socket.v2;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by vx on 05.03.14 .
 * Time: 17:28
 */
public class ConnectionMonitor implements Runnable {
    private static Logger logger = Logger.getLogger(ConnectionMonitor.class.getName());
    private int sleepTime;
    private int connected = 0;

    public ConnectionMonitor(int sleepTime) {
        this.sleepTime = sleepTime;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                connected = BankServerThreaded.getInstance().getConnectionCount();
                System.out.println("Concurrently connected: " + connected);
                logger.fine("Concurrently connected: " + connected);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
