package com.luxoft.bankapp.socket.v2;

import com.luxoft.bankapp.socket.v1.ServerService;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by vx on 05.03.14 .
 * Time: 16:50
 */
public class ServerThread implements Runnable {
    private long start;
    private Socket connection = null;
    private ServerService sservice;
    //    public static final String RESPONSE_HEADER = "HTTP/1.1 200 OK\n\n";
//    public static final String RESPONSE = "Welcome to multithreaded server!\n";
    private static Logger logger = Logger.getLogger(ServerThread.class.getName());

    public ServerThread(Socket socket) {
        this.connection = socket;
    }

    @Override
    public void run() {
        start = System.currentTimeMillis();
        // try - with - resources
        try (
                ObjectOutputStream outObj = new ObjectOutputStream(connection.getOutputStream());
                ObjectInputStream inObj = new ObjectInputStream(connection.getInputStream());
        ) {
            outObj.flush();
            // Adding connection to monitor
            BankServerThreaded.getInstance().incrementConnectionCount();

            logger.info("[" + Thread.currentThread().getName() + "] client connected [" +
                    new Date(start) +
                    "]");
            //sending response
            /**
             * Response is HTTP 1.1 format
             * you can test it in your browser!
             * just go to http://localhost:8094
             */
            //out.write((RESPONSE_HEADER + RESPONSE).getBytes());


            ServerService sservice = new ServerService(outObj, inObj);
            sservice.sendMessage("Connection successful");

            String message = "";
            do {
                try {
                    message = "";
                    try {
                        message = (String) inObj.readObject();
                    } catch (EOFException e) {
                        logger.severe("[" + Thread.currentThread().getName() + "](ServerThread) EOFException while reading command\n" +
                                e.getMessage());
                        break;
                    }
                    // System.out.println(message);

                    if (message.equals("closeConn")) {
                        //System.out.println("closeConn");
                        logger.fine("[" + Thread.currentThread().getName() + "](ServerThread) got closeConn message. Shutting down connection");
                        connection.shutdownOutput();
                    } else {
                        // server logic
                        switch (message) {

                            case "getClient":
                                sservice.getClient();
                                break;
                            case "withdraw":
                                logger.fine("[" + Thread.currentThread().getName() + "](ServerThread) Got withdraw command. Starting execution ");
                                sservice.withdraw();
                                break;
                            case "getBankInfo":
                                sservice.getBankInfo();
                                break;
                            case "getClientByName":
                                sservice.getClientByName();
                                break;
                            case "addClient":
                                sservice.addClient();
                                break;
                            case "deleteClient":
                                sservice.deleteClient();
                                break;
                            default:
                                // command not recognized
                                //System.out.println("client>" + message);

//                                message = "Unknown command";
//                                sendMessage(message);
                                break;

                        }
                    }
                } catch (ClassNotFoundException classnot) {
                    System.err.println("Data received in unknown format");
                }
            } while (!message.equals("closeConn"));

            //connection.shutdownInput();


        } catch (IOException ioException) {
            logger.severe("[" + Thread.currentThread().getName() + "](ServerThread) ioException caught\n" +
                    ioException.getMessage());
            ioException.printStackTrace();
        } finally {
            try {
                BankServerThreaded.getInstance().decrementConnectionCount();

                logger.info("[" + Thread.currentThread().getName() + "] client disconnected [" +
                        (System.currentTimeMillis() - start) +
                        "ms]");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // Closing connection


    }
}
