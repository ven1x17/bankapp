package com.luxoft.bankapp.socket.v2;

import com.luxoft.bankapp.domain.bank.BankInfo;
import com.luxoft.bankapp.domain.bank.Client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Logger;

/**
 * Created by vx on 06.03.14 .
 * Time: 11:15
 */
public class BankClient2 {
    private Socket requestSocket;
    private ObjectOutputStream outObj;
    private ObjectInputStream inObj;
    static Logger logger = Logger.getLogger(BankClient2.class.getName());

    private static final String HOST = "localhost";
    private int PORT = BankServerThreaded.PORT;

    public BankClient2(Socket requestSocket) throws IOException, ClassNotFoundException {
        this.requestSocket = requestSocket;

        // Getting Input and Output streams
        BufferedOutputStream outBuf = new BufferedOutputStream(requestSocket.getOutputStream());
        BufferedInputStream inBuf = new BufferedInputStream(requestSocket.getInputStream());
        outObj = new ObjectOutputStream(outBuf);
        outObj.flush();
        inObj = new ObjectInputStream(inBuf);
        logger.fine("[" + Thread.currentThread().getName() + "] (Client) streams opened");
        sendMessage("HELLO!");
        //System.outObj.println("server>" + (String) inObj.readObject());
        // read string
        String result = (String) inObj.readObject();
        logger.fine("[" + Thread.currentThread().getName() + "] (Client) got message from server: " + result);
    }

    @Deprecated
    public BankClient2() {
    }

    @Deprecated
    public void runTest() throws IOException {
        try {
            requestSocket = new Socket(HOST, PORT);
            System.out.println("Connected to " + HOST + ":" + PORT);
            // Getting Input and Output streams

            OutputStream out = requestSocket.getOutputStream();

            InputStream in = requestSocket.getInputStream();

            String message = "";
            // read first message
//            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\n");
//            for(int i=0; i<3;i++) {
//                if (s.hasNext())
//                message += s.next() + "\n";
//            }
//            System.out.println(message);

            outObj = new ObjectOutputStream(out);
            inObj = new ObjectInputStream(in);
            do {
                try {
                    sendMessage("HELLO!");
                    message = (String) inObj.readObject();
                    System.out.println("server>" + message);
                    // getting client info for clientId = 1
                    System.out.println("Sending getClient(id=1)");
                    message = "getClient";
                    sendMessage(message);
                    sendMessage("1");
                    Client result = (Client) inObj.readObject();
                    System.out.println("Got client:");
                    System.out.println(result);
                    message = "closeConn";
                    sendMessage(message);
                } catch (ClassNotFoundException classNot) {
                    System.err.println("data received in unknown format");
                }
            } while (!message.equals("closeConn"));
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            // Closing connection
            try {
                inObj.close();
                outObj.close();
                requestSocket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    @Deprecated
    public void connect() {
        try {
            requestSocket = new Socket(HOST, PORT);
            // Getting Input and Output streams
            outObj = new ObjectOutputStream(requestSocket.getOutputStream());
            inObj = new ObjectInputStream(requestSocket.getInputStream());

            sendMessage("HELLO!");
            //System.outObj.println("server>" + (String) inObj.readObject());
            // read string
            inObj.readObject();
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void connect(Socket conn) throws IOException, ClassNotFoundException {
        requestSocket = conn;
        // Getting Input and Output streams
        outObj = new ObjectOutputStream(requestSocket.getOutputStream());
        inObj = new ObjectInputStream(requestSocket.getInputStream());

        sendMessage("HELLO!");
        //System.outObj.println("server>" + (String) inObj.readObject());
        // read string
        inObj.readObject();
    }

    public Client getClient(int id) throws ClassNotFoundException {
        try {
            sendObject("getClient", String.valueOf(id));
//            sendMessage("getClient");
//            sendMessage(String.valueOf(id));
            Client result = (Client) inObj.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public Client getClient(String name) throws ClassNotFoundException {
        try {
            sendObject("getClientByName", name);
//            sendMessage("getClientByName");
//            sendMessage(name);
            Client result = (Client) inObj.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public String addClient(Client c) throws ClassNotFoundException {
        try {
            sendMessage("addClient");
            sendMessage(c);
            String result = (String) inObj.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public String deleteClient(String name) throws ClassNotFoundException {
        try {
            sendMessage("deleteClient");
            sendMessage(name);
            String result = (String) inObj.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public String withdraw(int clientId, Float amount) throws ClassNotFoundException {
        try {
            logger.fine("[" + Thread.currentThread().getName() + "] (Client) sending withdraw request: ");

            sendMessage("withdraw");
            sendObject(String.valueOf(clientId), String.valueOf(amount));
//            sendMessage("withdraw");
//            sendMessage(String.valueOf(clientId));
//            sendMessage(String.valueOf(amount));


            logger.fine("[" + Thread.currentThread().getName() + "] (Client) withdraw request sent. Waiting for response");
            String result = (String) inObj.readObject();
            logger.fine("[" + Thread.currentThread().getName() + "] (Client) got message from server: " + result);
            return result;
        } catch (IOException ioException) {
            logger.severe("[" + Thread.currentThread().getName() + "] (Client) IOException while waiting for withdrawal\n" +
                    ioException.getMessage());
        }
        return null;
    }

    public void closeConn() {
        try {
            sendMessage("closeConn");
            requestSocket.shutdownInput();
            requestSocket.shutdownOutput();

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            inObj.close();

            if (!requestSocket.isClosed()) {
                outObj.close();
                requestSocket.close();
            }
        } catch (IOException ioException) {
            logger.severe("[" + Thread.currentThread().getName() + "] (Client) Exception while trying to close connection\n"
                    + ioException.getMessage());
        }
    }

    void sendMessage(final Object msg) throws IOException {
        outObj.writeObject(msg);
        outObj.flush();
        outObj.reset();
        //System.outObj.println("client>" + msg);
    }

    private void sendObject(Object... objects) {
        try {
            for (Object object : objects) {
                outObj.writeObject(object);
            }
            outObj.flush();
            outObj.reset();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public BankInfo getBankInfo() throws ClassNotFoundException {
        try {
            sendMessage("getBankInfo");
            BankInfo result = (BankInfo) inObj.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    private static String readStreamToString(java.io.InputStream is, String messageEndDelimiter) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter(messageEndDelimiter);
        return s.hasNext() ? s.next() : "";
    }
}
