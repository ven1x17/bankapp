package com.luxoft.bankapp.socket.v1;

import com.luxoft.bankapp.domain.bank.BankInfo;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.service.bank.BankService;
import com.luxoft.bankapp.service.bank.PersistenceService;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Created by vx on 05.03.14 .
 * Time: 16:55
 */
public class ServerService {
    private ObjectOutputStream out;
    private ObjectInputStream in;
    Logger logger = Logger.getLogger(ServerService.class.getName());

    public ServerService(ObjectOutputStream out, ObjectInputStream in) {
        this.out = out;
        this.in = in;
    }

    public void deleteClient() throws IOException, ClassNotFoundException {
        //System.out.println("deleteClient request");
        String name = (String) in.readObject();
        try {
            PersistenceService.getInstance().selectClientByName(name);
            PersistenceService.getInstance().removeSelectedClientFromDb();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (BankNotSetException e) {
            e.printStackTrace();
        }
        out.writeObject("Client successfully removed from database");
        out.flush();
        //System.out.println("server> client removed");
    }

    public void addClient() throws IOException, ClassNotFoundException {
        //System.out.println("addClient request");
        Client c = (Client) in.readObject();
        try {
            BankService.getInstance().addClientToDb(c);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (BankNotSetException e) {
            e.printStackTrace();
        }
        out.writeObject("Client successfully added to database");
        out.flush();
        out.reset();
        //System.out.println("server> client added");
    }

    public void getClientByName() throws IOException, ClassNotFoundException {
        // System.out.println("getClientByName request");
        String name = (String) in.readObject();
        Client result = null;
        try {
            result = PersistenceService.getInstance().findClientByName(name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // send serialized client
        out.writeObject(result);
        out.flush();
        out.reset();
        //System.out.println("server> client info sent");
    }

    public void getBankInfo() throws IOException {
        //System.out.println("getBankInfo request");
        BankInfo result = null;
        try {
            result = PersistenceService.getInstance().getBankInfo();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        out.writeObject(result);
        out.flush();
        out.reset();
        //System.out.println("server> bank info sent");
    }

    public void getClient() throws IOException, ClassNotFoundException {
        // read client id Integer
        //System.out.println("getClient request");

        Integer id = Integer.parseInt((String) in.readObject());
        Client result = null;
        try {
            result = PersistenceService.getInstance().getClientById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // send serialized client
        out.writeObject(result);
        out.flush();
        out.reset();
        //System.out.println("server> client info sent");
    }

    public void withdraw() throws IOException, ClassNotFoundException {
        //System.out.println("withdraw request");

        Integer clientId = Integer.parseInt((String) in.readObject());
        Float amount = Float.parseFloat((String) in.readObject());
        logger.fine("[" + Thread.currentThread().getName() + "](ServerService) withdrawal started");
        Client selected = null;
        try {
            synchronized (ServerService.class) {
                selected = PersistenceService.getInstance().getClientById(clientId);
                selected.withdraw(amount);
            }
            logger.fine("[" + Thread.currentThread().getName() + "](ServerService) server side withdrawal successful");
        } catch (SQLException e) {
            logger.severe("[" + Thread.currentThread().getName() + "] SQLException " + e.getMessage());
            sendMessage("Something bad happened with database");
        } catch (NotEnoughFundsException e) {
            sendMessage(e.getMessage());
        }

        sendMessage("Withdrawal was successfull");
    }

    public void sendMessage(final String msg) throws IOException {
        out.writeObject(msg);
        out.flush();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        out.reset();
        //System.out.println("server>" + msg);
    }
}
