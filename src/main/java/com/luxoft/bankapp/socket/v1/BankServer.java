package com.luxoft.bankapp.socket.v1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by vx on 26.02.14 .
 * Time: 13:47
 */
public class BankServer {
    private Socket connection = null;

    private static final int PORT = 8094;

    public void run() {
        // try - with - resources
        try (ServerSocket providerSocket = new ServerSocket(PORT);
             ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
             ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
        ) {

            System.out.println("Waiting for connection");
            connection = providerSocket.accept();
            System.out.println("Connection received from "
                    + connection.getInetAddress().getHostName());
            // Getting Input and Output streams

            out.flush();


            ServerService sservice = new ServerService(out, in);

            sservice.sendMessage("Connection successful");

            String message = "";
            do {
                try {
                    message = (String) in.readObject();

                    //sendMessage(message + "[from server]");
                    if (message.equals("closeConn")) {
                        sservice.sendMessage("closeConn");
                    } else {
                        // server logic
                        switch (message) {
                            case "getClient":
                                sservice.getClient();
                                break;
                            case "withdraw":
                                sservice.withdraw();
                                break;
                            case "getBankInfo":
                                sservice.getBankInfo();
                                break;
                            case "getClientByName":
                                sservice.getClientByName();
                                break;
                            case "addClient":
                                sservice.addClient();
                                break;
                            case "deleteClient":
                                sservice.deleteClient();
                                break;
                            default:
                                // command not recognized
                                System.out.println("client>" + message);
//                                message = "Unknown command";
//                                sendMessage(message);
                                break;
                        }
                    }
                } catch (ClassNotFoundException classnot) {
                    System.err.println("Data received in unknown format");
                }
            } while (!message.equals("closeConn"));
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
