package com.luxoft.bankapp.socket.v1;

import com.luxoft.bankapp.domain.bank.BankInfo;
import com.luxoft.bankapp.domain.bank.Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by vx on 26.02.14 .
 * Time: 13:47
 */
public class BankClient {
    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;


    private static final String HOST = "localhost";
    private static final int PORT = 8080;

    public void runTest() throws IOException {
        try {
            requestSocket = new Socket(HOST, PORT);
            System.out.println("Connected to " + HOST + ":" + PORT);
            // Getting Input and Output streams
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(requestSocket.getInputStream());

            String message = "";
            do {
                try {
                    sendMessage("HELLO!");
                    message = (String) in.readObject();
                    System.out.println("server>" + message);
                    // getting client info for clientId = 1
                    System.out.println("Sending getClient(id=1)");
                    message = "getClient";
                    sendMessage(message);
                    sendMessage(new Integer(1));
                    Client result = (Client) in.readObject();
                    System.out.println("Got client:");
                    System.out.println(result);
                    message = "closeConn";
                    sendMessage(message);
                } catch (ClassNotFoundException classNot) {
                    System.err.println("data received in unknown format");
                }
            } while (!message.equals("closeConn"));
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            // Closing connection
            try {
                in.close();
                out.close();
                requestSocket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    public void connect() {
        boolean connected = false;
        while (!connected) {
            try {
                requestSocket = new Socket(HOST, PORT);
            } catch (IOException e) {
            }
        }

        try {
            // Getting Input and Output streams
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(requestSocket.getInputStream());
            sendMessage("HELLO!");
            //System.out.println("server>" + (String) in.readObject());
            // read string
            in.readObject();
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void connect(Socket conn) {
        requestSocket = conn;
        try {
            // Getting Input and Output streams
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(requestSocket.getInputStream());
            sendMessage("HELLO!");
            //System.out.println("server>" + (String) in.readObject());
            // read string
            in.readObject();
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Client getClient(int id) throws ClassNotFoundException {
        try {
            sendMessage("getClient");
            sendMessage(new Integer(id));
            Client result = (Client) in.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public Client getClient(String name) throws ClassNotFoundException {
        try {
            sendMessage("getClientByName");
            sendMessage(name);
            Client result = (Client) in.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public String addClient(Client c) throws ClassNotFoundException {
        try {
            sendMessage("addClient");
            sendMessage(c);
            String result = (String) in.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public String deleteClient(String name) throws ClassNotFoundException {
        try {
            sendMessage("deleteClient");
            sendMessage(name);
            String result = (String) in.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public String withdraw(int clientId, Float amount) throws ClassNotFoundException {
        try {
            sendMessage("withdraw");
            sendMessage(new Integer(clientId));
            sendMessage(amount);
            String result = (String) in.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public void closeConn() {
        try {
            sendMessage("closeConn");
            in.close();
            out.close();
            requestSocket.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    void sendMessage(final Object msg) throws IOException {
        out.writeObject(msg);
        out.flush();
        //System.out.println("client>" + msg);
    }

    public BankInfo getBankInfo() throws ClassNotFoundException {
        try {
            sendMessage("getBankInfo");
            BankInfo result = (BankInfo) in.readObject();
            return result;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }
}
