package com.luxoft.bankapp.exception;


public class NotEnoughFundsException extends BankException {
    private float amount;

    public NotEnoughFundsException(String message, float amount) {
        super(message);
        this.amount = amount;
    }

    public NotEnoughFundsException(String message) {
        super(message);
    }
}
