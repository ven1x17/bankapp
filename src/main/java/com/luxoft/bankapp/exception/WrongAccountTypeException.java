package com.luxoft.bankapp.exception;

public class WrongAccountTypeException extends BankException {

    public WrongAccountTypeException(String message) {
        super(message);
    }

}
