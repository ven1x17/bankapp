package com.luxoft.bankapp.exception;

public class AccountNotFoundException extends BankException {

    public AccountNotFoundException(String message) {
        super(message);
    }

}
