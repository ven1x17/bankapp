package com.luxoft.bankapp.exception;

/**
 * Created by User on 17.02.14.
 */
public class BankNotSetException extends BankException {
    public BankNotSetException(String message) {
        super(message);
    }
}
