package com.luxoft.bankapp.exception;


import com.luxoft.bankapp.domain.bank.Account;

public class OverDraftLimitExceededException extends NotEnoughFundsException {
    Account account;

    public OverDraftLimitExceededException(String message) {
        super(message);
    }

    public OverDraftLimitExceededException(Account account, String message, float amount) {
        super(message, amount);
        this.account = account;
    }

    public OverDraftLimitExceededException(Account account, String message) {
        super(message);
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }
}
