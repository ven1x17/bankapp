package com.luxoft.bankapp.exception;

/**
 * Created with IntelliJ IDEA.
 * User: vx
 * Date: 2/17/14
 * Time: 9:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommandInterruptedException extends BankException {
    public CommandInterruptedException(String message) {
        super(message);
    }
}
