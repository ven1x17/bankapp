package com.luxoft.bankapp.exception;


public class ClientExistsException extends BankException {

    public ClientExistsException(String message) {
        super(message);
    }

}
