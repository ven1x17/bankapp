package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.exception.BankNotSetException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by vx on 25.02.14 .
 * Time: 16:50
 */
public class BankFeedService {

    private static List<String> readFileToString(String filename) throws IOException {
        List<String> result = new ArrayList<>();
        FileReader fr = new FileReader(filename);
        BufferedReader br = new BufferedReader(fr);

        String temp = br.readLine();

        while (temp != null) {
            result.add(temp);
            temp = br.readLine();
        }
        br.close();
        fr.close();
        return result;
    }

    public static void loadFeed(String filename) throws IOException, BankNotSetException, SQLException, ClassNotFoundException {
        List<String> file = readFileToString(filename);
        Map<String, String> map;
        List<String> line;
        for (String l : file) { // each line
            map = new HashMap<>();
            line = Arrays.asList(l.split(";"));
            for (String param : line) { // each param
                String[] p = param.split("=");
                map.put(p[0], p[1]);
            }
            BankService.getInstance().getSelectedBank().parseFeed(map);
        }
    }
}
