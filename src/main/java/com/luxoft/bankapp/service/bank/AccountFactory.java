package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.AccountType;
import com.luxoft.bankapp.domain.bank.CheckingAccount;
import com.luxoft.bankapp.domain.bank.SavingAccount;
import com.luxoft.bankapp.exception.WrongAccountTypeException;

@Deprecated
public class AccountFactory {
    //
    public static final float DEFAULT_OVERDRAFT = 300;

    public static Account createAccountDefaultOverdraft(String accountType) throws WrongAccountTypeException {
        switch (accountType) {
            case "Saving":
            case "saving":
                return new SavingAccount(0);
            case "Checking":
            case "checking":
                CheckingAccount acc = new CheckingAccount(0);
                acc.setOverdraft(DEFAULT_OVERDRAFT);
                return acc;
            default:
                throw new WrongAccountTypeException(accountType + " is not supported");
        }
    }

    public static Account createAccount(String accountType, float initialOverdraft) throws WrongAccountTypeException {
        switch (accountType) {
            case "Saving":
            case "saving":
                return new SavingAccount(0);
            case "Checking":
            case "checking":
                CheckingAccount acc = new CheckingAccount(0);
                acc.setOverdraft(initialOverdraft);
                return acc;
            default:
                throw new WrongAccountTypeException(accountType + " is not supported");
        }
    }

    public static Account createAccountDefaultOverdraft(AccountType accountType) throws WrongAccountTypeException {
        switch (accountType) {
            case SAVING:
                return new SavingAccount(0);
            case CHECKING:
                CheckingAccount acc = new CheckingAccount(0);
                acc.setOverdraft(DEFAULT_OVERDRAFT);
                return acc;
            default:
                throw new WrongAccountTypeException(accountType + " is not supported");
        }
    }

    public static Account createAccount(AccountType accountType, float initialOverdraft) throws WrongAccountTypeException {
        switch (accountType) {
            case SAVING:
                return new SavingAccount(0);
            case CHECKING:
                CheckingAccount acc = new CheckingAccount(0);
                acc.setOverdraft(initialOverdraft);
                return acc;
            default:
                throw new WrongAccountTypeException(accountType + " is not supported");
        }
    }
}
