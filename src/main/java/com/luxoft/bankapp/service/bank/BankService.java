package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.ClientExistsException;
import org.springframework.stereotype.Service;

import java.io.*;
import java.sql.SQLException;

/**
 * Created by User on 12.02.14.
 */

/**
 * Singleton service for most bank methods
 * must be constructed with Bank instance before usage or otherwise
 * throws BankNotSetException
 */
@Service
public class BankService {

    private static BankService instance = null;
    private Bank bank;
    private Client selectedClient = null;
    private Bank selectedBank = null;

    @Deprecated
    public BankService(Bank bank) {
        this.bank = bank;
        this.selectedBank = bank;
        instance = this;
    }

    /**
     * Only for database access
     */
    public BankService() {
        instance = this;
    }

    public static BankService getInstance() throws BankNotSetException {
        if (instance == null) {
            throw new BankNotSetException("BankService is not initialized");
        } else {
            return instance;
        }
    }

    @Deprecated
    public Bank getBank() {
        return bank;
    }

    @Deprecated
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Client getSelectedClient() {
        return selectedClient;
    }

    public void setSelectedClient(Client selectedClient) {
//        if (bank.getClients().contains(selectedClient)){
        this.selectedClient = selectedClient;
//        } else {
//            //TODO: throw exception ! OR NOT
//            this.selectedClient = selectedClient;
//        }
    }

    @Deprecated
    public void setSelectedClient(String selectedClientName) {
        for (Client c : bank.getClients()) {
            if (c.getName().equals(selectedClientName)) {
                this.selectedClient = c;
            }
        }
        // if not found
        if (!selectedClient.getName().equals(selectedClientName)) {
            //TODO: throw exception
        }
    }

    @Deprecated
    public void addClient(Client c) throws ClientExistsException, BankNotSetException, SQLException, ClassNotFoundException {
        if (clientExists(c)) {
            throw new ClientExistsException("This client already exists");
        } else {
            bank.addClient(c);
        }
    }

    public void addClientToDb(Client c) throws SQLException, ClassNotFoundException {
        PersistenceService.getInstance().saveClient(c);
    }

    @Deprecated
    public boolean clientExists(String name) {
        return bank.getClientsMap().containsKey(name);
    }

    @Deprecated
    public boolean clientExists(Client client) {
        return bank.getClients().contains(client);
    }

    public boolean clientExistsInDb(String name) throws SQLException, ClassNotFoundException {
        Client c = PersistenceService.getInstance().findClientByName(name);
        if (c == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Finds client in the bank by name
     *
     * @param name
     * @return if no client found returns null
     */
    @Deprecated
    public Client getClientByName(String name) {
        return bank.getClientsMap().get(name);

        //old method
//        for(Client c:bank.getClients()){
//            if (c.getName().equals(name)){
//                return c;
//            }
//        }
//        return null;
    }

    public Client getClientByNameFromDb(String name) throws SQLException, ClassNotFoundException {
        return PersistenceService.getInstance().findClientByName(name);
    }

    public Bank getSelectedBank() {
        return selectedBank;
    }

    public void setSelectedBank(Bank selectedBank) {
        this.bank = selectedBank;
        this.selectedBank = selectedBank;
    }

//    public void printMaximumAmountToWithdraw() {
//        for (Client c : bank.getClients()) {
//            c.getClientSalutation();
//            System.out.println(c.getAccounts().get(0).maximumAmountToWithdraw());
//        }
//    }

    public Client loadClient(String filename) throws IOException, ClassNotFoundException {
        Client client = null;
        ObjectInputStream input = new ObjectInputStream(
                new FileInputStream(filename));
        client = (Client) input.readObject();
        input.close();
        return client;
    }

    public void saveClient(Client client, String filename) throws IOException {
        ObjectOutputStream output = new ObjectOutputStream(
                new FileOutputStream(filename));
        output.writeObject(client);
        output.close();
    }
}
