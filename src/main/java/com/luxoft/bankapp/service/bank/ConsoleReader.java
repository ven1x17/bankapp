package com.luxoft.bankapp.service.bank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: vx
 * Date: 2/17/14
 * Time: 8:41 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Singleton service for querying user input
 */
public class ConsoleReader {

    private static ConsoleReader instance = null;
    private BufferedReader br;

    public ConsoleReader() {
        br = new BufferedReader(new InputStreamReader(System.in));
        instance = this;
    }

    public String readLine() throws IOException {
        return br.readLine();
    }

    public static ConsoleReader getInstance() {
        if (instance != null) {
            return instance;
        } else {
            return new ConsoleReader();
        }
    }

    public void closeReaderStream() throws IOException {
        br.close();
    }
}
