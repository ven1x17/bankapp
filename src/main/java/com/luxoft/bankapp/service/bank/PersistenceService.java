package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.dao.*;
import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.BankInfo;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotSetException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

/**
 * User: vx(home)
 * Date: 2/23/14
 * Time: 7:11 PM
 */

/**
 * Singleton service for bank data persistance
 */
@Service
public class PersistenceService {
    private static PersistenceService instance = null;
    Logger logger = Logger.getLogger(PersistenceService.class.getName());

    @Autowired
    private ClientDao clientDao;
    @Autowired
    private BankDao bankDao;
    @Autowired
    private AccountDao accountDao;

    private Connection conn;

    public PersistenceService() throws ClassNotFoundException, SQLException {
        instance = this;

        bankDao = new H2BankDao();
        accountDao = new H2AccountDao();
        clientDao = new H2ClientDao(accountDao);
    }

    public PersistenceService(PersistenceService ps) {
        instance = ps;
    }

    public static PersistenceService getInstance() throws SQLException, ClassNotFoundException {
        if (instance == null) {
            return new PersistenceService();
        } else {
            return instance;
        }
    }

    public ClientDao getClientDao() {
        return clientDao;
    }

    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    public BankDao getBankDao() {
        return bankDao;
    }

    public void setBankDao(BankDao bankDao) {
        this.bankDao = bankDao;
    }

    public AccountDao getAccountDao() {
        return accountDao;
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public void initDatabase() throws SQLException, ClassNotFoundException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) initializing/recreating database");
        Class.forName("org.h2.Driver");
        conn = DriverManager.getConnection("jdbc:h2:bankapp", "sa", "");
        try {
            Statement st = conn.createStatement();
            st.execute("DROP TABLE IF EXISTS banks;");
            st.execute("DROP TABLE IF EXISTS accounts;");
            st.execute("DROP TABLE IF EXISTS clients;");
            st.execute("CREATE TABLE `banks` (\n" +
                    "bankId int primary key auto_increment,\n" +
                    "address varchar(255)\n" +
                    ");");
            st.execute("create table `clients` (\n" +
                    "clientId int primary key auto_increment,\n" +
                    "name varchar(255) not null,\n" +
                    "gender varchar(255),\n" +
                    "email varchar(255),\n" +
                    "phone varchar(255),\n" +
                    "city varchar(255),\n" +
                    "activeAccountId int,\n" +
                    "bankId int,\n" +
                    "foreign key (bankId) references `banks`(bankId)\n" +
                    ");");
            st.execute("create table `accounts` (\n" +
                    "accountId int primary key auto_increment,\n" +
                    "type varchar(255),\n" +
                    "balance float,\n" +
                    "overdraft float,\n" +
                    "max_overdraft float,\n" +
                    "clientId int,\n" +
                    "foreign key (clientId) references `clients`(clientId)\n" +
                    ")");


            st.execute("INSERT INTO `banks` (`address`) VALUES ('Moscow, Russia');");
            st.execute("INSERT INTO `clients` (`name`, `gender`, \n" +
                    "`email`, `phone`, `city`, `activeAccountId`, `bankId`) \n" +
                    "VALUES ('Hanna', 'female', 'hanna@somamail.com', '+312312312312', 'New york', '1', '1');\n" +
                    "\n" +
                    "INSERT INTO `clients` (`name`, `gender`, \n" +
                    "`email`, `phone`, `city`, `activeAccountId`, `bankId`) \n" +
                    "VALUES ('John', 'male', 'john@somamail.com', '+375867583495', 'New york', '3', '1');\n" +
                    "\n" +
                    "INSERT INTO `clients` (`name`, `gender`, \n" +
                    "`email`, `phone`, `city`, `activeAccountId`, `bankId`) \n" +
                    "VALUES ('Ben', 'male', 'ben@somamail.com', '+336495630457', 'New york', '4', '1');");

            st.execute("INSERT INTO `accounts` (`type`, `balance`, `overdraft`, `max_overdraft`, `clientId`) \n" +
                    "VALUES ('checking', '2000', '0', '50000', '1');\n" +
                    "INSERT INTO `accounts` (`type`, `balance`, `overdraft`, `clientId`) \n" +
                    "VALUES ('saving', '25000', '0', '1');\n" +
                    "INSERT INTO `accounts` (`type`, `balance`, `overdraft`, `max_overdraft`, `clientId`) \n" +
                    "VALUES ('checking', '0', '1200', '5000', '2');\n" +
                    "INSERT INTO `accounts` (`type`, `balance`, `overdraft`, `clientId`) \n" +
                    "VALUES ('checking', '3000', '0', '3');\n");
        } finally {
            conn.close();
        }
    }

    public void saveClient(Client c) throws SQLException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) save client command");
        clientDao.save(c);
    }

    public Client findClientByName(String name) throws SQLException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) find client by name command");
        return clientDao.getClientByName(name);
    }

    public List<Client> findClientByNameAndCity(String name, String city) throws SQLException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) find client by name and city command");
        return clientDao.getClientByNameAndCity(name, city);
    }

    public void selectBankById(int id) throws SQLException, BankNotSetException {
        BankService.getInstance().setSelectedBank(bankDao.getBankById(id));
    }

    public void selectClientByName(String name) throws SQLException, BankNotSetException {
        BankService.getInstance().setSelectedClient(clientDao.getClientByName(name));
    }

    public Client getClientById(int id) throws SQLException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) get client command");
        return new H2ClientDao(new H2AccountDao()).getClientById(id);
    }

    public void removeSelectedClientFromDb() throws BankNotSetException, SQLException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) remove selected client from db command");
        clientDao.remove(BankService.getInstance().getSelectedClient());
    }

    public void saveAccount(Account acc) throws SQLException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) save account command");
        new H2AccountDao().save(acc);
    }

    public List<Client> getAllClients() throws SQLException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) get all clients command");
        return clientDao.getAllClients();
    }

    public BankInfo getBankInfo() throws SQLException, ClassNotFoundException {
        logger.fine("[" + Thread.currentThread().getName() + "] (Persistance service) get bank info command");
        return bankDao.getBankInfo();
    }
}
