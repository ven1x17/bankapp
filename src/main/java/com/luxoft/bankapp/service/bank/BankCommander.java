package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.command.*;

import java.util.HashMap;

/**
 * Created by User on 17.02.14.
 */
public class BankCommander {
    private static HashMap<Integer, Command> commandsMap;

    public void registerCommand(Integer name, Command command) {
        commandsMap.put(name, command);
    }

    public void removeCommand(Integer name) {
        commandsMap.remove(name);
    }

    private static void initializeBasicCommands() {
        commandsMap = new HashMap<Integer, Command>();
        commandsMap.put(0, new TestH2DBCommand());
        commandsMap.put(1, new FindClientCommand());
        commandsMap.put(2, new AddClientCommand());
        commandsMap.put(3, new GetAccountsCommand());
        commandsMap.put(4, new DepositCommand());
        commandsMap.put(5, new WithdrawCommand());
        commandsMap.put(6, new TransferCommand());
        commandsMap.put(7, new BankReportFromDbCommand());
        commandsMap.put(8, new TestSerializeCommand());
    }

//    private static Command[] commands = {
//            new FindClientCommand(), // 1
//            new AddClientCommand(),  // 2
//            new GetAccountsCommand(),// 3
//            new DepositCommand(),    // 4
//            new WithdrawCommand(),   // 5
//            new TransferCommand(),   // 6
//    };

    /**
     * @throws java.io.IOException, BankException, SQLException, ClassNotFoundException
     */
    public static void startCommander() throws Exception {
        initializeBasicCommands();
        while (true) {
            System.out.println("======================================");
            System.out.println("Welcome to bank application interface!" + System.lineSeparator() + "Chose command from list below" +
                    System.lineSeparator() + "[To exit type \"exit\"]");

            for (int i = 0; i < commandsMap.size(); i++) {
                System.out.print((i) + ") ");
                commandsMap.get(i).printCommandInfo();
            }

            readCommandFromConsole();
        }
    }

    private static void readCommandFromConsole() throws Exception {
        try {
            String commandString = ConsoleReader.getInstance().readLine();
            if (commandString.equals("exit")) {
                ConsoleReader.getInstance().closeReaderStream();
                System.exit(0);
            }
            int command = Integer.parseInt(commandString);

            if (command > commandsMap.size() | command < 0) {
                System.err.println("Command not found!");
            } else {
                commandsMap.get(command).execute();
            }
        } catch (NumberFormatException e) {
            System.err.println("Invalid command!");
        }
    }
}
