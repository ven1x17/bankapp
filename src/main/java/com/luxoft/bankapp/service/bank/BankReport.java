package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.CheckingAccount;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotSetException;

import java.util.*;

/**
 * Created by User on 18.02.14.
 */
public class BankReport {
    public static void getNumberOfClients() throws BankNotSetException {
        System.out.println(BankService.getInstance().getBank().getClients().size());
    }

    public static void getAccountNumber() throws BankNotSetException {
        int num = 0;
        for (Client c : BankService.getInstance().getBank().getClients()) {
            for (Account acc : c.getAccounts()) {
                num++;
            }
        }
        System.out.println("Number of active accounts: " + num);
    }

    public static void getClientsSorted() throws BankNotSetException {
        //sorting is made in tree set by comparator
        Set<Client> clientsTree = BankService.getInstance().getBank().getClients();

        System.out.println("List of clients sorted by current account balance");
        for (Client c : clientsTree) {
            System.out.println(c);
        }
    }

    public static void getBankCreditSum() throws BankNotSetException {
        // i have no idea what this is "supposed" to do
        double overdrafts = 0;
        for (Client c : BankService.getInstance().getBank().getClients()) {
            for (Account acc : c.getAccounts()) {
                if (acc.getClass() == CheckingAccount.class) {
                    overdrafts += ((CheckingAccount) acc).getOverdraft();
                }
            }
        }
        System.out.println("Credit given by bank: " + overdrafts);
    }

    public static void getClientByCity() throws BankNotSetException {
        Map<String, List<Client>> citymap = new HashMap<String, List<Client>>();
        for (Client c : BankService.getInstance().getBank().getClients()) {
            if (citymap.containsKey(c.getCity())) {
                citymap.get(c.getCity()).add(c);
            } else {
                ArrayList<Client> cl = new ArrayList<Client>();
                cl.add(c);
                citymap.put(c.getCity(), cl);
            }
        }
    }


}
