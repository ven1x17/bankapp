package com.luxoft.bankapp.testing;

import com.luxoft.bankapp.testing.annotations.NoDB;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * Created by vx on 28.02.14 .
 * Time: 16:12
 */
public class TestService {

    public static boolean isEquals(Object o1, Object o2) throws IllegalAccessException {
        boolean result = false;
        Object f1 = null;
        Object f2 = null;
        Class clazz = o1.getClass();

        HashMap<String, Field> map = new HashMap<>();

        for (Field field : clazz.getDeclaredFields()) {
            if (!field.isAnnotationPresent(NoDB.class)) {
                map.put(field.getName(), field);
            }
        }
        Class clazz2 = o2.getClass();
        for (Field field2 : clazz2.getDeclaredFields()) {
            if (map.containsKey(field2.getName())) {
                map.get(field2.getName()).setAccessible(true);
                f1 = map.get(field2.getName()).get(o1);
                field2.setAccessible(true);
                f2 = field2.get(o2);
                if (f1 != null) {
                    if (f2 != null) {
                        if (!f1.equals(f2)) return false;
                    } else return false;
                } else if (f2 != null) return false;
            }
        }
        return true;
    }
}
