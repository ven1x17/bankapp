package com.luxoft.bankapp.testing.MultithreadedChat;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by vx on 07.03.14 .
 * Time: 18:11
 */
public class ChatMain {


    public static void main(String[] args) {

        ChatMain m = new ChatMain();

        if (args.length > 0) {
            switch (args[0]) {
                case "-client":
                    ChatClient.clientInterface();
                    break;
                case "-server":
                    Thread serverThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ChatServer server = new ChatServer();
                                server.startListening();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    serverThread.start();
                    break;
                case "-test":
                    concurrentClientsTest();
                    break;
            }
        }
    }


    private static AtomicInteger clientsThatPassedTest = new AtomicInteger(0);

    private static void incrementPassed() {
        clientsThatPassedTest.getAndIncrement();
    }

    private static void concurrentClientsTest() {
        int clientCount = 10000;
        ExecutorService es = Executors.newFixedThreadPool(10);

        class clientThread implements Runnable {
            String name;

            clientThread(String name) {
                this.name = name;
            }

            @Override
            public void run() {
                try {
                    if (ChatClient.testClientInterface(name)) {
                        incrementPassed();
                    }
                    ;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        for (int i = 0; i < clientCount; i++) {
            Thread t = new Thread(new clientThread(String.valueOf(i)));
            es.execute(t);
        }

        try {
            es.shutdown();
            es.awaitTermination(10000, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("===================================");
        if (clientsThatPassedTest.get() == clientCount) {
            System.out.println("TEST PASSED");
        } else {
            System.out.println("TEST FAILED : passed = " + clientsThatPassedTest.get() + "/" + clientCount);
        }
    }
}
