package com.luxoft.bankapp.testing.MultithreadedChat;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.luxoft.bankapp.service.bank.ConsoleReader;

/**
 * Created by vx on 07.03.14 .
 * Time: 18:12
 */
public class ChatClient {
    private static final String HOST = "localhost";
    public static final int PORT = 8080;
    private Socket conn;
    private ObjectOutputStream outObj;
    private ObjectInputStream inObj;

    private String name = "anon";

    public ChatClient(Socket conn) throws IOException {
        this.conn = conn;
        // Getting Input and Output streams
//        BufferedOutputStream outBuf = new BufferedOutputStream(conn.getOutputStream());
//        BufferedInputStream inBuf = new BufferedInputStream(conn.getInputStream());

        outObj = new ObjectOutputStream(
                new BufferedOutputStream(
                        conn.getOutputStream()
                ));
        outObj.flush();
        inObj = new ObjectInputStream(
                new BufferedInputStream(
                        conn.getInputStream()));

        System.out.println("Connected");
        sendObject("conn");
    }

    public static void clientInterface() {
        try (
                Socket conn = new Socket(HOST, PORT);
        ) {
            ChatClient chat = new ChatClient(conn);

            System.out.println("Enter your username: [default:'anon']");
            String name = ConsoleReader.getInstance().readLine();
            if (!"".equals(name)) {
                chat.setName(name);
            }
            // send username
            chat.sendObject(chat.getName());

            //starting server message reader
            Thread chatReader = startReader(chat);

            System.out.println("You can now chat! [to exit type 'exit']");
            String message = "Hello!";
            while (!"exit".equals(message)) {
                chat.sendObject("chat", message);
                message = ConsoleReader.getInstance().readLine();
            }
            chat.sendObject("closeConn");
            conn.shutdownOutput();
            chatReader.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static boolean testClientInterface(String name) throws InterruptedException {
        try (
                Socket conn = new Socket(HOST, PORT);
        ) {
            ChatClient chat = new ChatClient(conn);
            chat.setName(name);
            chat.sendObject(chat.getName());
            //starting server message reader
            Thread chatReader = startReader(chat);

            String message = "Hello!";
            chat.sendObject("chat", message, "test message");

//            Thread.sleep(100);
            chat.sendObject("closeConn");
            conn.shutdownInput();

            chatReader.interrupt();
//            Thread.sleep(200);
            conn.shutdownOutput();
            chat.close();
            chatReader.join();
            System.out.println(name + " has passed test");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static Thread startReader(ChatClient chat) {
        Thread chatReader = new Thread(new ChatReader(chat));
        chatReader.setDaemon(true);
        chatReader.start();
        return chatReader;
    }

    private void close() throws IOException {
        inObj.close();
        if (!conn.isClosed()) {
            outObj.close();
            conn.close();
        }
    }

    public static class ChatReader implements Runnable {
        ChatClient chat;

        private ChatReader(ChatClient chat) {
            this.chat = chat;
        }

        @Override
        public void run() {
            while (!Thread.interrupted()) {
                try {
                    String msg = chat.getMessage();
                    if (msg != null)
                        System.out.println(msg);
                } catch (IOException e) {
//                    e.printStackTrace();
//                    System.err.println("Client exception while reading server response");
                    return;
                } catch (ClassNotFoundException e) {
//                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    private void sendObject(Object... objects) {
        try {
            for (Object object : objects) {
                outObj.writeObject(object);
            }
            outObj.flush();
            outObj.reset();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getMessage() throws IOException, ClassNotFoundException {
        return (String) inObj.readObject();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
