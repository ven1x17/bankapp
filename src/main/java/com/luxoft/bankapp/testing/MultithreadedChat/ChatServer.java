package com.luxoft.bankapp.testing.MultithreadedChat;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by vx on 07.03.14 .
 * Time: 18:12
 */
public class ChatServer {
    public static final int PORT = 8080;
    public static final int IDLE_CONNECTIONS_LIMIT = 10000;
    private int poolSize = 10;
    private final ServerSocket serverSocket;
    private final ExecutorService pool;
    private final ExecutorService announcer = Executors.newFixedThreadPool(1);
    private String message = "";

    private static ChatServer instance;
    ConcurrentSkipListMap<String, ObjectOutputStream> clients = new ConcurrentSkipListMap<>();

    public ChatServer() throws IOException {
        serverSocket = new ServerSocket(PORT, IDLE_CONNECTIONS_LIMIT);
        pool = Executors.newFixedThreadPool(poolSize);
        instance = this;
    }

    public static ChatServer getInstance() {
        return instance;
    }

    /**
     * Main server method
     */
    public void startListening() throws IOException {
        System.out.println("Chat server started on port " + PORT);
        try {
            while (!Thread.interrupted()) {
                Socket clientSocket = serverSocket.accept();
                pool.execute(new ChatThread(clientSocket));
            }
        } finally {
            serverSocket.close();
        }
    }


    private class ChatThread implements Runnable {
        private Socket conn = null;

        public ChatThread(Socket conn) {
            this.conn = conn;
        }

        @Override
        public void run() {
            String name = "";
            try (
                    ObjectOutputStream outObj = new ObjectOutputStream(
                            //new BufferedOutputStream(
                            conn.getOutputStream());
                    ObjectInputStream inObj = new ObjectInputStream(
                            //new BufferedInputStream(
                            conn.getInputStream());
            ) {

                while (!conn.isInputShutdown()) {
                    message = "";
                    message = getMessage(inObj);

                    switch (message) {
                        case "conn":
                            name = getMessage(inObj);
                            if ("anon".equals(name)) {
                                name += outObj.hashCode();
                            }
                            if (clients.containsKey(name)) {
                                name += outObj.hashCode();
                            }
                            ChatServer.getInstance().addClientToMap(name, outObj);
                            System.out.println("client connected. Current client list:" +
                                    Arrays.toString(clients.keySet().toArray()));
                            ChatServer.getInstance().anounceMessage(
                                    name, name + " connected!");
                            break;
                        case "chat":
                            message = getMessage(inObj);
                            if (message != null) {
                                ChatServer.getInstance().anounceMessage(
                                        name, message);
                                System.out.println(name + ": " + message);
                            } else {
                                // when client disconnects
                                // he spams null messages in chat mode
                                // for some reason
                                removeClientFromMap(name);
                                anounceMessage("", name + " disconnected");
                                conn.shutdownInput();
                                break;
                            }
                            break;
                        case "closeConn":
                            removeClientFromMap(name);
                            anounceMessage("", name + " left");
                            conn.shutdownInput();
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();

                removeClientFromMap(name);
                anounceMessage("", name + " disconnected");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    conn.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private String getMessage(ObjectInputStream in)
                throws IOException, ClassNotFoundException {
            return (String) in.readObject();
        }
    }

    private void anounceMessage(final String senderName, final String message) {
        //should add message to queue executor of 1

        announcer.execute(
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Map<String, ObjectOutputStream> newMap;
                        synchronized (this) {
                            newMap = new HashMap<>(clients);
                        }
                        //list of clients
//                System.out.println(Arrays.toString(clients.keySet().toArray()));

                        if (newMap.containsKey(senderName)) {
                            newMap.remove(senderName);
                        }

                        List<Thread> clientSenderThreads = new ArrayList<>();

                        for (final ObjectOutputStream s : newMap.values()) {
                            // send message to every connected client via separate thread
                            Thread tmp = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        s.writeObject(senderName + ": " + message);
                                        s.flush();
                                        Thread.sleep(200);

                                        s.reset();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            clientSenderThreads.add(tmp);
                            tmp.start();
                        }

                        for (Thread t : clientSenderThreads) {
                            try {
                                t.join();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        //log.info("announced [" + senderName + ": " + message + "] to " +
//                        Arrays.toString(newMap.keySet().toArray()));
                    }
                }));
    }

    public synchronized void addClientToMap(String name, ObjectOutputStream out) {
        clients.put(name, out);
    }

    public void removeClientFromMap(String name) {
        synchronized (this) {
            if (clients.containsKey(name)) {
                clients.remove(name);
            }
        }
    }
}
