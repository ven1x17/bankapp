package com.luxoft.bankapp.testing.tests;

import com.luxoft.bankapp.dao.ClientDao;
import com.luxoft.bankapp.dao.H2AccountDao;
import com.luxoft.bankapp.dao.H2ClientDao;
import com.luxoft.bankapp.domain.bank.Account;
import com.luxoft.bankapp.domain.bank.AccountType;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.Gender;
import com.luxoft.bankapp.exception.WrongAccountTypeException;
import com.luxoft.bankapp.service.bank.PersistenceService;
import com.luxoft.bankapp.testing.TestService;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by vx on 28.02.14 .
 * Time: 18:34
 */
public class ClientDaoTest {
    Client client;
    ClientDao clientDao = new H2ClientDao(new H2AccountDao());

    @Before
    public void initClient() throws WrongAccountTypeException, SQLException {
        //initialize client with accounts
        client = new Client("Samuel", Gender.MALE, "samuel@m.com", "1231231231", 1000f);
        client.addAccount(AccountType.CHECKING);
    }

    @Test
    public void testInsert() throws SQLException, ClassNotFoundException {
        //save client
        PersistenceService ps = new PersistenceService();
        ps.initDatabase();

        clientDao.save(client);
    }


    @Test
    public void testUpdate() throws SQLException, IllegalAccessException {
        // clone client and make changes
        Client newClient = new Client(client.getName(),
                client.getGender(),
                client.getEmail(),
                client.getPhone(),
                client.getInitialOverdraft());
        newClient.addAccount((Account) client.getAccounts().toArray()[0]);
        newClient.setEmail("newemail@m.com");
        // resave client (client is saved by name)
        clientDao.save(newClient);
        //reload client
        Client fromDb = clientDao.getClientByName(client.getName());

        assertFalse(TestService.isEquals(client, fromDb));
        assertTrue(TestService.isEquals(newClient, fromDb));
    }
}
