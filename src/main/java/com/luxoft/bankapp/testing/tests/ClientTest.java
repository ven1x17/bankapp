package com.luxoft.bankapp.testing.tests;

import com.luxoft.bankapp.socket.v2.BankClient2;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by vx on 06.03.14 .
 * Time: 11:24
 */
public class ClientTest {
    private BankClient2 client2;

    @Before
    public void init() {
        client2 = new BankClient2();
    }

    @Test
    public void testRun() throws IOException {
        client2.runTest();
    }
}
