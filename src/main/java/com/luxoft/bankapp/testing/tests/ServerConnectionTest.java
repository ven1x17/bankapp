package com.luxoft.bankapp.testing.tests;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.main.BankApplication;
import com.luxoft.bankapp.service.bank.PersistenceService;
import com.luxoft.bankapp.socket.v2.BankClient2;
import com.luxoft.bankapp.socket.v2.BankServerThreaded;
import com.luxoft.bankapp.socket.v2.ConnectionMonitor;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Created by vx on 05.03.14 .
 * Time: 17:57
 */
public class ServerConnectionTest {
    private BankClient2 bankClient;
    private Set<Future> list = new HashSet<>();
    private static final String HOST = "localhost";
    private final int PORT = BankServerThreaded.PORT;
    static Logger logger = Logger.getLogger(ServerConnectionTest.class.getName());

    private long avgResponseTime = 0;
    private Future f;
    private ExecutorService es = Executors.newFixedThreadPool(50);
    private int clientCount = 10;

    @Before
    public void init() throws BankNotSetException, SQLException, ClassNotFoundException, IOException {
        try {
            BankApplication.initializeSingletons();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // starting connection monitor
        new Thread(new ConnectionMonitor(100)).start();

        System.out.println("Connection monitor active");
        System.out.println("Server v2 is listening on " + PORT);
        new Thread(new Runnable() {
            @Override
            public void run() {
                BankServerThreaded server = null;
                try {
                    server = new BankServerThreaded();
                    server.startThreads();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @Test
    public void test() throws IllegalAccessException, IOException, ClassNotFoundException, InterruptedException, ExecutionException {

        Client client = null;
        try {
            client = PersistenceService.getInstance().getClientById(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        double amount = client.getBalance();

        class clientConn implements Callable {
            Socket clientSocket = null;
            long start;

            clientConn() {
                start = System.currentTimeMillis();
            }

            @Override
            public Object call() {
                try (
                        Socket clientSocket = new Socket(HOST, PORT);
                ) {
                    bankClient = new BankClient2(clientSocket);
                    //Client c = bankClient.getClient(1);
                    String result = bankClient.withdraw(1, 1f);
                    bankClient.closeConn();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
//                finally {
//                    bankClient.closeConn();
//                }
                long elapsed = System.currentTimeMillis() - start;

                return elapsed;
            }
        }

        for (int i = 0; i < clientCount; i++) {
            f = es.submit(new clientConn());
            list.add(f);
            Thread.sleep(5);
        }

        for (Future future : list) {
            avgResponseTime += (long) future.get();
        }
        System.out.println("Average client waiting time: " + (avgResponseTime / clientCount));

        try {
            client = PersistenceService.getInstance().getClientById(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        double amount2 = client.getBalance();
        Assert.assertEquals((amount - clientCount), amount2);
        // adding back 1000
        try {
            client.deposit(1000f);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // stop server
//        BankServerThreaded.getInstance().stop();
    }
}
