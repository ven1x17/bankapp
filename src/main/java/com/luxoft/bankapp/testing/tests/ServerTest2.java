package com.luxoft.bankapp.testing.tests;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.main.BankApplication;
import com.luxoft.bankapp.service.bank.PersistenceService;
import com.luxoft.bankapp.socket.v2.BankClient2;
import com.luxoft.bankapp.socket.v2.BankServerThreaded;
import com.luxoft.bankapp.socket.v2.ConnectionMonitor;
import junit.framework.Assert;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Created by vx on 07.03.14 .
 * Time: 16:08
 */
public class ServerTest2 {

    private static BankClient2 bankClient;
    private static Set<Future> list = new HashSet<>();
    private static final String HOST = "localhost";
    private static final int PORT = BankServerThreaded.PORT;
    static Logger logger = Logger.getLogger(ServerConnectionTest.class.getName());

    private static long avgResponseTime = 0;
    private static Future f;
    private static ExecutorService es = Executors.newFixedThreadPool(50);
    private static int clientCount = 100;

    public static void init() throws BankNotSetException, SQLException, ClassNotFoundException, IOException {
        try {
            BankApplication.initializeSingletons();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // starting connection monitor
        new Thread(new ConnectionMonitor(100)).start();

        System.out.println("Connection monitor active");
        System.out.println("Server v2 is listening on " + PORT);
        new Thread(new Runnable() {
            @Override
            public void run() {
                BankServerThreaded server = null;
                try {
                    server = new BankServerThreaded();
                    server.startThreads();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    public static void main(String[] args) {

        try {
            init();
        } catch (BankNotSetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Client client = null;
        try {
            client = PersistenceService.getInstance().getClientById(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        double amount = client.getBalance();

        class clientConn implements Callable {
            Socket clientSocket = null;
            long start;

            clientConn() {
                start = System.currentTimeMillis();
            }

            @Override
            public Object call() {
                try (
                        Socket clientSocket = new Socket(HOST, PORT);
                ) {
                    bankClient = new BankClient2(clientSocket);

                    //Client c = bankClient.getClient(1);
                    String result = bankClient.withdraw(1, 1f);
                    bankClient.closeConn();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
//                finally {
//                    bankClient.closeConn();
//                }
                return System.currentTimeMillis() - start;
            }
        }

        for (int i = 0; i < clientCount; i++) {
            f = es.submit(new clientConn());
            list.add(f);
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (Future future : list) {
            try {
                avgResponseTime += (long) future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Average client waiting time: " + (avgResponseTime / clientCount));

        try {
            try {
                client = PersistenceService.getInstance().getClientById(1);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        double amount2 = client.getBalance();
        Assert.assertEquals((amount - clientCount), amount2);
        // adding back 1000
        try {
            client.deposit(1000f);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        // stop server
        try {
            BankServerThreaded.getInstance().stop();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
