package com.luxoft.bankapp.testing.tests;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.ClientRegistrationListener;
import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.testing.Mocks.BankMock;
import com.luxoft.bankapp.testing.TestService;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by vx on 28.02.14 .
 * Time: 17:41
 */
public class TestServiceTest {
    Bank bank1, bank2;

    @Before
    public void initBanks() throws BankNotSetException, SQLException {
        bank1 = new Bank(new ArrayList<ClientRegistrationListener>());
        bank1.setId(1);
        bank1.setName("My Bank");

        //NODB field
        List<ClientRegistrationListener> listeners = new ArrayList<ClientRegistrationListener>();
        listeners.add(new ClientRegistrationListener() { //client printer
            @Override
            public void onClientAdded(Client c) {
                c.printReport();
            }
        });

        bank2 = new BankMock(listeners);
        bank2.setId(1);
        bank2.setName("My Bank");

    }

    @Test
    public void testEquals() throws IllegalAccessException {
        assertTrue(TestService.isEquals(bank1, bank2));
    }

}
