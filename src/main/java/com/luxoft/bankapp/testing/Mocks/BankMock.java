package com.luxoft.bankapp.testing.Mocks;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.ClientRegistrationListener;

import java.util.List;

/**
 * Created by vx on 28.02.14 .
 * Time: 17:45
 */
public class BankMock extends Bank {

    private int id;
    private String name;
    private List<ClientRegistrationListener> listeners;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public BankMock(List<ClientRegistrationListener> listeners) {
        super(listeners);
    }
}
