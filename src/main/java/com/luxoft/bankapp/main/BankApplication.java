/**
 *
 */
package com.luxoft.bankapp.main;

import com.luxoft.bankapp.domain.bank.*;
import com.luxoft.bankapp.domain.bank.command.SocketClientCommand;
import com.luxoft.bankapp.exception.*;
import com.luxoft.bankapp.service.bank.*;
import com.luxoft.bankapp.socket.v1.BankClient;
import com.luxoft.bankapp.socket.v1.BankServer;
import com.luxoft.bankapp.socket.v2.BankServerThreaded;
import com.luxoft.bankapp.socket.v2.ConnectionMonitor;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BankApplication {
    /**
     * BankApp's only entry point
     *
     * @param args args[0] can be:
     *             -report prints bank report
     *             -client starts in remote client mode (on localhost:8094)
     *             -server starts BankApp server (on localhost:8094)
     */
    public static void main(String[] args) {
        try {
            if (args.length > 0) {
                parseArgs(args);
            } else {
                Bank bank = new Bank(getBasicListeners());
                PersistenceService data = new PersistenceService();
                BankService bankService = new BankService(bank);
                //initialize(bank);
                //modifyBank(bank);
                //printBankReport(bank);
                BankCommander.startCommander();
                //BankService.printMaximumAmountToWithdraw(bank);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (OverDraftLimitExceededException e) {
            System.out.println("Withdraw request cannot exceed " + e.getAccount().maximumAmountToWithdraw());
        } catch (BankException e) {
            // handle exception
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void parseArgs(String[] args) throws SQLException, ClassNotFoundException, IOException, BankNotSetException, CommandInterruptedException, ClientExistsException, WrongAccountTypeException {
        if ("-report".equals(args[0])) {
            Bank bank = new Bank(new ArrayList<ClientRegistrationListener>());
            PersistenceService data = new PersistenceService();
            BankService bankService = new BankService(bank);
            //initialize(bank);
            //modifyBank(bank);
            BankReport.getAccountNumber();
            BankReport.getBankCreditSum();
            BankReport.getClientByCity();
            BankReport.getClientsSorted();
            BankReport.getNumberOfClients();
        } else if ("-server".equals(args[0])) {
            initializeSingletons();
            BankServer server = new BankServer();
            System.out.println("Server is listening on port 8094");
            while (true) {
                server.run();
            }
        } else if ("-client".equals(args[0])) {
            BankClient client = new BankClient();
            client.runTest();
            new SocketClientCommand().execute();
        } else if ("-server2".equals(args[0])) {
            initializeSingletons();
            BankServerThreaded server = new BankServerThreaded();
            new Thread(new ConnectionMonitor(1000)).start();
            System.out.println("Connection monitor active");
            System.out.println("Server v2 is listening on " + BankServerThreaded.PORT);
            server.startThreads();
        } else if ("-spring".equals(args[0])) {
            //BankCommander configured with spring
            AbstractApplicationContext context = new ClassPathXmlApplicationContext("classpath:application-context.xml");
            com.luxoft.bankapp.spring.BankCommander commander = (com.luxoft.bankapp.spring.BankCommander) context.getBean("bankCommander");
            //initializing BankService for BankCommander
            new BankService(new Bank(new ArrayList<ClientRegistrationListener>()));
            PersistenceService.getInstance().selectBankById(1);
            // making bank from database context aware
            Bank dbBank = BankService.getInstance().getSelectedBank();
            BankService.getInstance().setSelectedBank(((Bank) context.getBean("bank")));
            Bank selectedBank = BankService.getInstance().getSelectedBank();
            selectedBank.setAddress(dbBank.getAddress());
            selectedBank.setId(dbBank.getId());


            try {
                commander.startCommander();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void initializeSingletons() throws ClassNotFoundException, SQLException {
        Bank bank = new Bank(getBasicListeners());
        PersistenceService data = new PersistenceService();
        BankService bankService = new BankService(bank);
        try {
            data.selectBankById(1);
        } catch (BankNotSetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes bank with some clients
     *
     * @param b
     * @throws AccountNotFoundException
     * @throws WrongAccountTypeException
     * @throws SQLException
     */
    @Deprecated
    public static void initialize(Bank b) throws AccountNotFoundException, WrongAccountTypeException, SQLException {
        try {
            Client c1 = new Client("Alex", Gender.MALE);
            c1.addAccount(AccountType.SAVING);
            BankService.getInstance().addClient(c1);
            Client c2 = new Client("Hanna", Gender.FEMALE);
            c2.addAccount(AccountType.CHECKING);
            BankService.getInstance().addClient(c2);
            Client c3 = new Client("John", Gender.MALE);
            c3.addAccount(AccountFactory.createAccountDefaultOverdraft(AccountType.SAVING));
            BankService.getInstance().addClient(c3);
            c1.deposit(5000);
            c2.deposit(1200);
            c3.deposit(100000);

        } catch (BankException e) {
            // handle exception
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates client registration listeners
     *
     * @return
     */
    public static List<ClientRegistrationListener> getBasicListeners() {
        List<ClientRegistrationListener> listeners = new ArrayList<ClientRegistrationListener>();

        listeners.add(new ClientRegistrationListener() { //client printer
            @Override
            public void onClientAdded(Client c) {
                c.printReport();
            }
        });

        listeners.add(new ClientRegistrationListener() { //email notification
            @Override
            public void onClientAdded(Client c) {
                System.out.println("Notification email for " + c + " to be sent");
            }
        });

        listeners.add(new ClientRegistrationListener() { //email notification
            @Override
            public void onClientAdded(Client c) {
                System.out.println("Notification email for " + c + " to be sent");
            }
        });

        listeners.add(new ClientRegistrationListener() { //map addition
            @Override
            public void onClientAdded(Client c) throws BankNotSetException {
                BankService.getInstance().getBank().getClientsMap().put(c.getName(), c);
            }
        });
        return listeners;
    }

    public static void printBankReport(Bank b) {
        b.printReport();
    }

    @Deprecated
    public static void modifyBank(Bank b) throws AccountNotFoundException, NotEnoughFundsException, SQLException, ClassNotFoundException {
//        Account a = b.getClients().get(0).getAccounts().get(0);
//        b.getClients().get(0).setActiveAccount(a);

        Iterator iter = b.getClients().iterator();
        while (iter.hasNext()) {
            ((Client) iter.next()).deposit(500f);
        }
    }
}
