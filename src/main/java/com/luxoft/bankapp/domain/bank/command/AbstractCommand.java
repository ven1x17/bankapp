package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.service.bank.ConsoleReader;

import java.io.IOException;

/**
 * Created by User on 17.02.14.
 */
public abstract class AbstractCommand implements Command {

    /**
     * Gets data to ask user to input (for example gender)
     *
     * @param questionMessage asks user this question before input
     * @param regex           regex of input to be checked for validity before returning value
     */
    protected String getInput(String questionMessage, String regex) throws IOException, CommandInterruptedException {
        System.out.println(questionMessage);
        System.out.println("[To exit type \"exit\"]");
        while (true) {
            try {
                String command = ConsoleReader.getInstance().readLine();
                if (command.equals("exit")) {
                    throw new CommandInterruptedException("");
                }

                if (command.matches(regex)) {
                    return command;
                } else {
                    System.err.println("Invalid input!");
                }
            } catch (IllegalArgumentException e) {
                System.err.println("Invalid param input!");
            }
        }

    }
}
