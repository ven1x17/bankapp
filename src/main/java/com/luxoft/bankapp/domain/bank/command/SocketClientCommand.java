package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.ClientRegistrationListener;
import com.luxoft.bankapp.exception.*;
import com.luxoft.bankapp.service.bank.BankService;
import com.luxoft.bankapp.socket.v1.BankClient;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by vx on 26.02.14 .
 * Time: 15:21
 */
public class SocketClientCommand extends AbstractCommand {
    @Override
    public void execute() throws BankNotSetException, IOException, ClientExistsException, WrongAccountTypeException, CommandInterruptedException, SQLException {
        System.out.println("==============================");
        System.out.println("Welcome to remote bank interface:");
        // ask client id [or go into bankRemoteOffice mode]
        String id = getInput("Enter id of client you want to work with: " +
                "[BankRemoteOffice mode: type \"BRO\"]", "[0-9|BRO]{1,}");

        BankClient bc = new BankClient();
        bc.connect();
        try {
            if (!id.equals("BRO")) {
                // get client from database
                Client result = null;

                result = bc.getClient(Integer.parseInt(id));
                System.out.println("Selected client:");
                System.out.println(result);
                // ask for command {getBalance, withdraw}
                System.out.println("==============================");
                System.out.println("1. balance");
                System.out.println("2. Withdraw");
                System.out.println("==============================");
                String command = getInput("Enter command:", "[1|2]");
                switch (command) {
                    case "1":
                        System.out.println("balance = " + result.getBalance());
                        break;
                    case "2":
                        BankService bs = new BankService(new Bank(new ArrayList<ClientRegistrationListener>()));
                        BankService.getInstance().setSelectedClient(result);
                        Float amount = customWithdrawCommand();
                        // if withdraw is acceptable resend it to server
                        String response = bc.withdraw(Integer.parseInt(id), amount);
                        System.out.println("Server> " + response);
                        break;
                }

            } else {
                //BankRemoteOffice interface
                System.out.println("==============================");
                System.out.println("1. Bank info");
                System.out.println("2. Find client by name");
                System.out.println("3. Add client");
                System.out.println("4. Delete client");
                System.out.println("==============================");
                String command = getInput("Enter command:", "[1-4]");
                switch (command) {
                    case "1":
                        System.out.println(bc.getBankInfo());
                        break;
                    case "2":
                        command = getInput("Enter name:", ".{2,}");
                        System.out.println(bc.getClient(command));
                        break;
                    case "3":
                        // custom get client command call
                        // send add client command to server
                        break;
                    case "4":
                        // custom select client command call
                        // send delete client command to server
                        break;
                }

            }
        } catch (ClassNotFoundException e) {
            System.err.println("Wrong version. You need to update your software!");
        }
        bc.closeConn();
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Socket client interface");
    }

    private float customWithdrawCommand() throws IOException, CommandInterruptedException {
        System.out.println("==============================");
        System.out.println("Input <Withdraw> parameters");
        float d = Float.parseFloat(getInput("Enter withdraw amount (xx.xx):", "[0-9]+[.][0-9]+"));
        try {
            BankService.getInstance().getSelectedClient().withdraw(d);
        } catch (NotEnoughFundsException e) {
            System.err.println(e.getMessage());
            customWithdrawCommand();
        } catch (SQLException e) {
            // do nothing, because we have no database
        } catch (BankNotSetException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return d;
    }
}
