package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.exception.WrongAccountTypeException;
import com.luxoft.bankapp.service.bank.BankService;
import com.luxoft.bankapp.service.bank.PersistenceService;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by vx on 25.02.14 .
 * Time: 18:21
 */
public class TestSerializeCommand extends AbstractCommand {

    @Override
    public void execute() throws BankNotSetException, IOException, ClientExistsException, WrongAccountTypeException, CommandInterruptedException, SQLException, ClassNotFoundException {

        PersistenceService.getInstance().selectClientByName("Jack");
        Client selected = BankService.getInstance().getSelectedClient();
        System.out.println("Selected for serialization client:");
        System.out.println(selected);

        String filename = "client.file";
        BankService.getInstance().saveClient(selected, filename);
        System.out.println("Client serialized successfully!");
        Client deserialized = null;
        try {
            deserialized = BankService.getInstance().loadClient(filename);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Client deserialized successfully:");
        System.out.println(deserialized);
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Test serialization/deserialization of client");
    }
}
