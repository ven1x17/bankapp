package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.service.bank.PersistenceService;
import com.luxoft.bankapp.spring.ClientAddedEvent;
import com.luxoft.bankapp.testing.annotations.NoDB;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.sql.SQLException;
import java.util.*;

public class Bank implements Report, ApplicationContextAware {

    private int id;
    private String address;
    private ApplicationContext context;
    @NoDB
    private Set<Client> clients = new TreeSet<Client>(new Comparator<Client>() {
        @Override
        public int compare(Client client, Client client2) {
            return Float.compare(client.getBalance(), client2.getBalance());
        }
    });
    @NoDB
    private List<ClientRegistrationListener> listeners;
    @NoDB
    private Map<String, Client> clientsMap = new HashMap<String, Client>();
    private String name;

    public Bank(List<ClientRegistrationListener> listeners) {
        super();
        this.listeners = listeners;
    }

    /**
     * For spring init
     */
    public Bank() {
        this.listeners = new ArrayList<ClientRegistrationListener>();
    }

    public Bank(int id, String address, List<ClientRegistrationListener> listeners) {
        this.id = id;
        this.address = address;
        this.listeners = listeners;
    }

    @Override
    public void printReport() {
        System.out.println("<<<BANK REPORT>>>");
        for (Client c : clients) {
            c.printReport();
        }
        System.out.println("<<< / BANK REPORT>>>");
        System.out.println();
    }

    public Set<Client> getClients() {
        //treeset has no constructor (obj, comparator)
        //but i need to be able to modify clients array from outside
        //return new TreeSet(clients);
        return clients;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

    public void addClient(Client c) throws BankNotSetException, SQLException, ClassNotFoundException {
        if (PersistenceService.getInstance() != null) {
            PersistenceService.getInstance().saveClient(c);
        }
        clients.add(c);
        for (ClientRegistrationListener listener : listeners) {
            listener.onClientAdded(c);
        }
        this.context.publishEvent(new ClientAddedEvent(c));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bank bank = (Bank) o;

        if (clients != null ? !clients.equals(bank.clients) : bank.clients != null) return false;
        if (!listeners.equals(bank.listeners)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = clients != null ? clients.hashCode() : 0;
        result = 31 * result + (listeners != null ? listeners.hashCode() : 0);
        return result;
    }

    public Map<String, Client> getClientsMap() {
        return clientsMap;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "id=" + id +
                ", address='" + address + '\'' +
                '}';
    }

    /**
     * Method adds new client from feed map
     *
     * @param feed parameter-value map
     * @throws SQLException
     * @throws BankNotSetException
     */
    public void parseFeed(Map<String, String> feed) throws SQLException, BankNotSetException, ClassNotFoundException {
        Client c = new Client(feed.get("name"),
                Gender.getGender(feed.get("gender")),
                feed.get("email"),
                feed.get("phone"),
                Float.parseFloat(feed.get("initialOverdraft")));
        if ("c".equals(feed.get("accounttype"))) {
            CheckingAccount ca = new CheckingAccount(0, 0,
                    Float.parseFloat(feed.get("balance")),
                    Float.parseFloat(feed.get("overdraft")),
                    Float.parseFloat(feed.get("maxOverdraft")));
            c.addAccount(ca);
        } else if ("s".equals(feed.get("accounttype"))) {
            SavingAccount sa = new SavingAccount(0,
                    Float.parseFloat(feed.get("balance")));
            c.addAccount(sa);
        }
        addClient(c);
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
