package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.service.bank.BankService;

import java.io.IOException;

/**
 * Created by User on 17.02.14.
 */
public class FindClientCommand extends AbstractCommand {
    @Override
    public void execute() throws IOException, BankNotSetException {
        System.out.println("==============================");
        System.out.println("Input <find client> parameters");
        try {
            String name = getInput("Enter client name: [To show client list type \"clientlist\"]", ".{2,}");
            if (name.equals("clientlist")) {
                BankService.getInstance().getBank().printReport();
                execute();
            } else if (BankService.getInstance().clientExists(name)) {
                BankService.getInstance().setSelectedClient(name);
                System.out.println("(INFO):Client found!");
                System.out.println("(INFO):Currently selected client:");
                System.out.println(BankService.getInstance().getSelectedClient());
            } else {
                System.out.println("(INFO):Client not found!");
                execute();
            }
        } catch (CommandInterruptedException e) {
            System.out.println("(INFO):Command interrupted!");
        }
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Select client");
    }
}
