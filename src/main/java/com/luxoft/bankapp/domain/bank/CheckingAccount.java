package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.exception.OverDraftLimitExceededException;

import java.sql.SQLException;
import java.util.Map;

public class CheckingAccount extends AbstractAccount {

    private float overdraft = 0;
    private float maxOverdraft;

    @Deprecated
    public CheckingAccount(float balance) {
        super(0, balance);
    }

    public CheckingAccount(int id, int clientId, float balance, float overdraft, float maxOverdraft) throws IllegalArgumentException {
        super(clientId, balance);
        this.setId(id);
        this.overdraft = overdraft;
        this.maxOverdraft = maxOverdraft;
    }

    public CheckingAccount(int clientId, float balance, float overdraft, float maxOverdraft) throws IllegalArgumentException {
        super(clientId, balance);
        this.overdraft = overdraft;
        this.maxOverdraft = maxOverdraft;
    }

    public void setOverdraft(float overdraft) throws IllegalArgumentException {
        if (overdraft < 0) {
            throw new IllegalArgumentException();
        }
        this.overdraft = overdraft;
    }

    public float getOverdraft() {
        return overdraft;
    }

    @Override
    public void printReport() {
        System.out.println("[Checking account: overdraft = " + overdraft + " | balance = " + getBalance() + "]");
    }

    @Override
    public void withdraw(float x) throws NotEnoughFundsException, SQLException, ClassNotFoundException {
        if (x > (getBalance() + (maxOverdraft - overdraft))) {
            throw new OverDraftLimitExceededException(this, "Operation not possible: Maximum overdraft will be exceeded");
        } else {
            float newBalance = getBalance() - x;
            if (newBalance >= 0) {
                super.withdraw(x);
            } else {
                overdraft -= newBalance; // new balance is negative
                // command causing sqlexception is last
                super.withdraw(getBalance());
            }
        }
    }

    @Override
    public void deposit(float x) throws SQLException, ClassNotFoundException {
        if (overdraft > 0) {
            overdraft = overdraft - x;
            if (overdraft < 0) {
                super.deposit(-overdraft);
                overdraft = 0;
            }
        }
    }

    @Override
    public float maximumAmountToWithdraw() {
        return getBalance() + overdraft;
    }

    @Override
    public String toString() {
        return "[Checking account: overdraft = " + overdraft + " | Balance = " + getBalance() + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CheckingAccount that = (CheckingAccount) o;

        if (Float.compare(that.maxOverdraft, maxOverdraft) != 0) return false;
        if (Float.compare(that.overdraft, overdraft) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (overdraft != +0.0f ? Float.floatToIntBits(overdraft) : 0);
        result = 31 * result + (maxOverdraft != +0.0f ? Float.floatToIntBits(maxOverdraft) : 0);
        return result;
    }

    public float getMaxOverdraft() {
        return maxOverdraft;
    }

    public void setMaxOverdraft(float maxOverdraft) {
        if (maxOverdraft < 0) {
            throw new IllegalArgumentException();
        }
        this.maxOverdraft = maxOverdraft;
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    public void parseFeed(Map<String, String> feed) throws SQLException, ClassNotFoundException {
        // this method makes no sense, we have nothing to batch parse inside account
        // maybe this class needs initialization from feed map?
        setClientId(Integer.parseInt(feed.get("clientId")));

        // as of now we cannot SET balance after the construction
        // we can deposit negative values so why not:
        deposit(Float.parseFloat(feed.get("balance")) - getBalance());

        overdraft = Float.parseFloat(feed.get("overdraft"));
        maxOverdraft = Float.parseFloat(feed.get("maxOverdraft"));
    }
}
