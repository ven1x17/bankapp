package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;

import java.io.Serializable;
import java.sql.SQLException;

public interface Account extends Report, Serializable {

    public float getBalance();

    public void deposit(float x) throws SQLException, ClassNotFoundException;

    public void withdraw(float x) throws NotEnoughFundsException, SQLException, ClassNotFoundException;

    public float maximumAmountToWithdraw();

    public int decimalValue();
}
