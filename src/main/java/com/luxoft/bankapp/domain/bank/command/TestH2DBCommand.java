package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.exception.WrongAccountTypeException;
import com.luxoft.bankapp.service.bank.BankService;
import com.luxoft.bankapp.service.bank.PersistenceService;

import java.io.IOException;
import java.sql.SQLException;

/**
 * User: vx(home)
 * Date: 2/23/14
 * Time: 9:11 PM
 */
public class TestH2DBCommand extends AbstractCommand {

    @Override
    public void execute() throws BankNotSetException, IOException, ClientExistsException, WrongAccountTypeException, CommandInterruptedException, ClassNotFoundException {
        try {
            PersistenceService.getInstance().selectBankById(1);
            System.out.println("Bank successfully selected:");
            System.out.println(BankService.getInstance().getSelectedBank());

//            PersistenceService.getInstance().saveClient(new Client("Jack", Gender.MALE,"jack@some.mail","+385769485734", 300f));
//            System.out.println("Client was successfully added to database:");
//            System.out.println(PersistenceService.getInstance().findClientByName("Jack"));

            PersistenceService.getInstance().selectClientByName("Jack");
            System.out.println("Currently selected client:");
            System.out.println(BankService.getInstance().getSelectedClient());

            PersistenceService.getInstance().selectClientByName("Hanna");
            System.out.println("Currently selected client:");
            System.out.println(BankService.getInstance().getSelectedClient());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void printCommandInfo() {
        System.out.println("Test database connectivity");
    }
}
