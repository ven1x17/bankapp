package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.service.bank.BankService;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by User on 17.02.14.
 */
public class DepositCommand extends AbstractCommand {
    @Override
    public void execute() throws IOException, CommandInterruptedException, BankNotSetException, SQLException, ClassNotFoundException {
        System.out.println("==============================");
        System.out.println("Input <Deposit> parameters");
        float d = Float.parseFloat(getInput("Enter deposit amount (xx.xx):", "[0-9]+[.][0-9]+"));
        BankService.getInstance().getSelectedClient().deposit(d);
        BankService.getInstance().getSelectedClient().printReport();
        System.out.println("(INFO):Deposit was successful!");
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Deposit money for selected client");
    }
}
