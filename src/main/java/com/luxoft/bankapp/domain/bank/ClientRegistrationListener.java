package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.BankNotSetException;

/**
 * Created with IntelliJ IDEA.
 * User: vx
 * Date: 2/12/14
 * Time: 11:15 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ClientRegistrationListener {
    public void onClientAdded(Client c) throws BankNotSetException;
}
