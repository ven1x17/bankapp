package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.service.bank.BankService;

/**
 * Created by User on 17.02.14.
 */
public class GetAccountsCommand extends AbstractCommand {
    @Override
    public void execute() throws BankNotSetException {
        System.out.println("==============================");
        System.out.println("Accounts of selected client:");
        BankService.getInstance().getSelectedClient().printReport();
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Show accounts of selected client");
    }
}
