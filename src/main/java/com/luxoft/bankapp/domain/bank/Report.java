package com.luxoft.bankapp.domain.bank;

public interface Report {

    public void printReport();
}
