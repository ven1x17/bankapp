package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.service.bank.BankService;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by User on 17.02.14.
 */
public class TransferCommand extends AbstractCommand {
    @Override
    public void execute() throws BankNotSetException, IOException, CommandInterruptedException, SQLException, ClassNotFoundException {
        System.out.println("==============================");
        System.out.println("Input <Transfer> parameters");

        String targetName = getTargetName();


        try {
            float d = Float.parseFloat(getInput("Enter amount to transfer (xx.xx):", "[0-9]+[.][0-9]+"));
            BankService.getInstance().getSelectedClient().withdraw(d);
            BankService.getInstance().getClientByName(targetName).deposit(d);
            BankService.getInstance().getClientByNameFromDb(targetName).deposit(d);
        } catch (NotEnoughFundsException e) {
            System.err.println(e.getMessage());
            execute();
        }
        BankService.getInstance().getSelectedClient().printReport();
        BankService.getInstance().getClientByName(targetName).printReport();
        System.out.println("(INFO):Transfer was successful!");
    }

    private String getTargetName() throws IOException, CommandInterruptedException, BankNotSetException, SQLException, ClassNotFoundException {
        String targetName = getInput("Enter transfer target client name: [To show client list type \"clientlist\"]", ".{2,}");
        if (targetName.equals("clientlist")) {
            BankService.getInstance().getBank().printReport();
            execute();
        } else if (BankService.getInstance().clientExists(targetName)) {
            System.out.println("(INFO):Client found!");
            System.out.println("(INFO):Currently selected transfer target client:");
            BankService.getInstance().getClientByName(targetName).printReport();
        } else {
            System.out.println("(INFO):Client not found!");
            execute();
        }
        return targetName;
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Transfer for selected client");
    }
}
