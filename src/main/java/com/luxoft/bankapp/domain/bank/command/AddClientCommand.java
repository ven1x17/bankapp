package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.domain.bank.AccountType;
import com.luxoft.bankapp.domain.bank.Client;
import com.luxoft.bankapp.domain.bank.Gender;
import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.exception.WrongAccountTypeException;
import com.luxoft.bankapp.service.bank.BankService;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by User on 17.02.14.
 */
public class AddClientCommand extends AbstractCommand {
    @Override
    public void execute() throws BankNotSetException, IOException, WrongAccountTypeException, SQLException {
        System.out.println("==============================");
        System.out.println("Input <Add client> parameters");
        try {
            String name = getInput("Enter client name:", ".{2,}");
            Gender gender = Gender.getGender(getInput("Enter gender : m or f", "[m|f]"));
            String email = getInput("Enter email :", "[a-z0-9]+[@][a-z0-9]+[.][a-z]{2,4}");
            String phone = getInput("Enter phone(+3xxxxxxxxxxx {11x}) :", "^[+][3][0-9]{11}");
            float overdraft = Float.parseFloat(getInput("Enter overdraft (xx.xx) :", "[0-9]+[.][0-9]+"));
            Client c = new Client(name, gender, email, phone, overdraft);

            BankService.getInstance().setSelectedClient(c);
            BankService.getInstance().getSelectedClient().addAccount(AccountType.CHECKING);
            BankService.getInstance().addClient(c);
//            BankService.getInstance().addClientToDb(c);
            System.out.println("(INFO):Client added! Client was assigned default checking account!");

        } catch (NumberFormatException e) {
            System.err.println("Invalid overdraft!");
            execute();
        } catch (ClientExistsException e) {
            System.out.println("(INFO):Client already exists!");
            execute();
        } catch (CommandInterruptedException e) {
            System.out.println("(INFO):Command interrupted!");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void printCommandInfo() {
        System.out.println("Add new client");
    }
}
