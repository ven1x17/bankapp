package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.service.bank.PersistenceService;

import java.sql.SQLException;


public abstract class AbstractAccount implements Account {

    private int id;
    private int clientId;
    private float balance;

    @Deprecated
    protected AbstractAccount(float balance) throws IllegalArgumentException {
        this.id = 0;
        clientId = 0;
        if (balance < 0) {
            throw new IllegalArgumentException();
        }
        this.balance = balance;
    }

    protected AbstractAccount(int id, int clientId, float balance) {
        this.id = id;
        this.clientId = clientId;
        this.balance = balance;
    }

    protected AbstractAccount(int clientId, float balance) {
        this.clientId = clientId;
        this.balance = balance;
    }

    @Override
    public float getBalance() {
        return balance;
    }

    @Override
    public int decimalValue() {
        return Math.round(balance);
    }

    @Override
    public void deposit(float x) throws SQLException, ClassNotFoundException {
        balance += x;
        PersistenceService.getInstance().saveAccount(this);
    }

    @Override
    public void withdraw(float x) throws NotEnoughFundsException, SQLException, ClassNotFoundException {
        if (x > balance) {
            throw new NotEnoughFundsException("Not enough funds");
        } else {
            balance -= x;
            if (PersistenceService.getInstance() != null) {
                PersistenceService.getInstance().saveAccount(this);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractAccount that = (AbstractAccount) o;

        if (Float.compare(that.balance, balance) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (balance != +0.0f ? Float.floatToIntBits(balance) : 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
