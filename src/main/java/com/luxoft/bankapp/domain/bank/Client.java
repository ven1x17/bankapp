package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.AccountNotFoundException;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.exception.WrongAccountTypeException;
import com.luxoft.bankapp.testing.annotations.NoDB;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Client implements Report, Serializable {

    @NoDB
    private int id;
    private int bankId = 1;
    private String name = "name";
    private Gender gender = Gender.MALE;
    private String email = "email";
    private String phone = "phone";
    @NoDB
    private String city = "no city specified";
    private Set<Account> accounts = new HashSet<Account>();
    @NoDB
    private Account activeAccount;
    @NoDB
    private float initialOverdraft = 300f;

    public Client(String name, Gender gender) {
        super();
        this.name = name;
        this.gender = gender;
        accounts = new HashSet<Account>();
        initialOverdraft = 300;
    }

    public Client(String name, Gender gender, String email, String phone, float initialOverdraft) {
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.initialOverdraft = initialOverdraft;
    }

    public Client(int id, int bankId, String name, Gender gender, String email, String phone) {
        this.id = id;
        this.bankId = bankId;
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Account> getAccounts() {
        return Collections.unmodifiableSet(accounts);
    }

    public void setActiveAccount(Account activeAccount) throws AccountNotFoundException {
        if (accounts.contains(activeAccount)) {
            this.activeAccount = activeAccount;
        } else {
            throw new AccountNotFoundException("Client " + name + " have no such account");
        }
    }

    public float getBalance() {
        return activeAccount.getBalance();
    }

    public void deposit(float x) throws SQLException, ClassNotFoundException {
        activeAccount.deposit(x);
    }

    public void withdraw(float x) throws NotEnoughFundsException, SQLException, ClassNotFoundException {
        activeAccount.withdraw(x);

    }

    /**
     * Active account is set automatically
     *
     * @param accountType
     * @throws WrongAccountTypeException
     */
    public void addAccount(AccountType accountType) throws WrongAccountTypeException, SQLException {
        Account acc;
        switch (accountType) {
            case SAVING:
                acc = new SavingAccount(this.id, 0);

                accounts.add(acc);
                activeAccount = acc;
                break;
            case CHECKING:
                acc = new CheckingAccount(this.id, 0, 0, initialOverdraft);

                accounts.add(acc);
                activeAccount = acc;
                break;
            default:
                throw new WrongAccountTypeException(accountType + " is not supported");
        }
        if (activeAccount == null) {
            activeAccount = acc;
        }
    }

    /**
     * Active account is set automatically
     *
     * @param acc
     */
    public void addAccount(Account acc) throws SQLException {

        accounts.add(acc);
        activeAccount = acc;
    }

    @Override
    public void printReport() {
        System.out.println(this);
    }

    public void getClientSalutation() {
        if (gender == Gender.FEMALE) {
            System.out.println("Hello " + gender.getGenderString() + " " + name);
        } else {
            System.out.println("Hello " + gender.getGenderString() + " " + name);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Client{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", initialOverdraft=" + initialOverdraft +
                "}, ");
        if (accounts == null || accounts.size() == 0) {
            sb.append("[ Accounts: no accounts]");
        } else {
            sb.append("[ Accounts:" + System.lineSeparator());
            for (Account a : accounts) {
                sb.append(a);
            }
            sb.append(" ]");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (Float.compare(client.initialOverdraft, initialOverdraft) != 0) return false;
        if (accounts != null ? !accounts.equals(client.accounts) : client.accounts != null) return false;
        if (gender != client.gender) return false;
        if (name != null ? !name.equals(client.name) : client.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (accounts != null ? accounts.hashCode() : 0);
        result = 31 * result + (initialOverdraft != +0.0f ? Float.floatToIntBits(initialOverdraft) : 0);
        return result;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getInitialOverdraft() {
        return initialOverdraft;
    }

    public void setInitialOverdraft(float initialOverdraft) {
        this.initialOverdraft = initialOverdraft;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Gender getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    /**
     * Parses account from map and adds it
     *
     * @param feed
     * @throws SQLException
     */
    public void parseFeed(Map<String, String> feed) throws SQLException {
        if ("c".equals(feed.get("accounttype"))) {
            CheckingAccount ca = new CheckingAccount(0,
                    this.id,
                    Float.parseFloat(feed.get("balance")),
                    Float.parseFloat(feed.get("overdraft")),
                    Float.parseFloat(feed.get("maxOverdraft")));
            addAccount(ca);
        } else if ("s".equals(feed.get("accounttype"))) {
            SavingAccount sa = new SavingAccount(0,
                    this.id,
                    Float.parseFloat(feed.get("balance")));
            addAccount(sa);
        }
    }
}
