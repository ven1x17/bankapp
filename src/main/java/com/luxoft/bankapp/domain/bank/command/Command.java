package com.luxoft.bankapp.domain.bank.command;

/**
 * Created by User on 17.02.14.
 */
public interface Command {
    public void execute() throws Exception;

    public void printCommandInfo();

}
