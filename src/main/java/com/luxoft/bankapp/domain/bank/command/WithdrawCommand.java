package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.exception.NotEnoughFundsException;
import com.luxoft.bankapp.service.bank.BankService;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by User on 17.02.14.
 */
public class WithdrawCommand extends AbstractCommand {

    @Override
    public void execute() throws IOException, CommandInterruptedException, BankNotSetException, SQLException {
        System.out.println("==============================");
        System.out.println("Input <Withdraw> parameters");
        float d = Float.parseFloat(getInput("Enter withdraw amount (xx.xx):", "[0-9]+[.][0-9]+"));
        try {
            BankService.getInstance().getSelectedClient().withdraw(d);
        } catch (NotEnoughFundsException e) {
            System.err.println(e.getMessage());
            execute();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        BankService.getInstance().getSelectedClient().printReport();
        System.out.println("(INFO):Withdrawal was successful!");
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Withdraw money for selected client");
    }
}
