package com.luxoft.bankapp.domain.bank;

import com.luxoft.bankapp.exception.NotEnoughFundsException;

import java.sql.SQLException;
import java.util.Map;

public class SavingAccount extends AbstractAccount {

    public SavingAccount(int id, int clientId, float balance) {
        super(id, clientId, balance);
    }

    public SavingAccount(int clientId, float balance) {
        super(clientId, balance);
    }

    @Deprecated
    public SavingAccount(float balance) {
        super(0, balance);
    }

    @Override
    public void printReport() {
        System.out.println("[Saving account: " + "balance = " + getBalance() + "]");
    }

    @Override
    public float getBalance() {
        return super.getBalance();
    }

    @Override
    public void deposit(float x) throws SQLException, ClassNotFoundException {
        super.deposit(x);
    }

    @Override
    public void withdraw(float x) throws NotEnoughFundsException, SQLException, ClassNotFoundException {
        super.withdraw(x);
    }

    @Override
    public float maximumAmountToWithdraw() {
        return getBalance();
    }

    @Override
    public String toString() {
        return "[Saving account: " + "balance = " + getBalance() + "]";
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    public void parseFeed(Map<String, String> feed) throws SQLException, ClassNotFoundException {
        // this method makes no sense, we have nothing to batch parse inside account
        // maybe this class needs initialization from feed map?

        setClientId(Integer.parseInt(feed.get("clientId")));

        // as of now we cannot SET balance after the construction
        // we can deposit negative values so why not:
        deposit(Float.parseFloat(feed.get("balance")) - getBalance());
    }
}
