package com.luxoft.bankapp.domain.bank.command;

import com.luxoft.bankapp.exception.BankNotSetException;
import com.luxoft.bankapp.exception.ClientExistsException;
import com.luxoft.bankapp.exception.CommandInterruptedException;
import com.luxoft.bankapp.exception.WrongAccountTypeException;
import com.luxoft.bankapp.service.bank.PersistenceService;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by vx on 24.02.14 .
 * Time: 16:55
 */
public class BankReportFromDbCommand extends AbstractCommand {
    @Override
    public void execute() throws BankNotSetException, IOException, ClientExistsException, WrongAccountTypeException, CommandInterruptedException, SQLException, ClassNotFoundException {
        System.out.println(PersistenceService.getInstance().getBankInfo());
    }

    @Override
    public void printCommandInfo() {
        System.out.println("Print bank report from database");
    }
}
