package com.luxoft.bankapp.domain.bank;

import java.io.Serializable;

/**
 * Created by User on 12.02.14.
 */
public enum Gender implements Serializable {
    MALE("Mr."), FEMALE("Mrs.");

    private String genderString;

    Gender(String genderString) {
        this.genderString = genderString;
    }

    public String getGenderString() {
        return genderString;
    }

    public static Gender getGender(String s) {
        if (s.toLowerCase().equals("male") || s.toLowerCase().equals("m")) {
            return Gender.MALE;
        } else {
            if (s.toLowerCase().equals("female") | s.toLowerCase().equals("f")) {
                return Gender.FEMALE;
            }
        }
        throw new IllegalArgumentException("Invalid argument string");
    }
}
