package com.luxoft.bankapp.spring;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;

/**
 * Created by vx on 20.03.2014 .
 * Time: 16:37
 */
public class PrintClientListener implements ApplicationListener<ClientAddedEvent> {
    private static Logger log = Logger.getLogger(PrintClientListener.class);

    @Override
    public void onApplicationEvent(ClientAddedEvent applicationEvent) {
        log.info("Client added " + applicationEvent.getSource());
        System.out.println("Client added " + applicationEvent.getSource());
    }
}
