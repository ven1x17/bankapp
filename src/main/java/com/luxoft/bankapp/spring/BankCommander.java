package com.luxoft.bankapp.spring;

import com.luxoft.bankapp.domain.bank.command.Command;
import com.luxoft.bankapp.service.bank.ConsoleReader;

import java.util.HashMap;

/**
 * Created by User on 17.02.14.
 */
public class BankCommander {
    private static HashMap<Integer, Command> commandsMap;

    public BankCommander() {
    }

    public static HashMap<Integer, Command> getCommandsMap() {
        return commandsMap;
    }

    public static void setCommandsMap(HashMap<Integer, Command> commandsMap) {
        BankCommander.commandsMap = commandsMap;
    }

    private static void readCommandFromConsole() throws Exception {
        try {
            String commandString = ConsoleReader.getInstance().readLine();
            if (commandString.equals("exit")) {
                ConsoleReader.getInstance().closeReaderStream();
                System.exit(0);
            }
            int command = Integer.parseInt(commandString);

            if (command > commandsMap.size() | command < 0) {
                System.err.println("Command not found!");
            } else {
                commandsMap.get(command).execute();
            }
        } catch (NumberFormatException e) {
            System.err.println("Invalid command!");
        }
    }

    public void registerCommand(Integer name, Command command) {
        commandsMap.put(name, command);
    }

    public void removeCommand(Integer name) {
        commandsMap.remove(name);
    }

    /**
     * @throws java.io.IOException, BankException, SQLException, ClassNotFoundException
     */
    public void startCommander() throws Exception {
        while (true) {
            System.out.println("======================================");
            System.out.println("Welcome to bank application interface!" + System.lineSeparator() + "Chose command from list below" +
                    System.lineSeparator() + "[To exit type \"exit\"]");

            for (int i = 1; i < commandsMap.size() + 1; i++) {
                System.out.print((i) + ") ");
                commandsMap.get(i).printCommandInfo();
            }

            readCommandFromConsole();
        }
    }
}
