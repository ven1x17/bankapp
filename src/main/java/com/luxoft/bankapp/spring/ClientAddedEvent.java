package com.luxoft.bankapp.spring;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationEvent;

/**
 * Created by vx on 20.03.2014 .
 * Time: 16:34
 */
public class ClientAddedEvent extends ApplicationEvent {
    private static Logger log = Logger.getLogger(ClientAddedEvent.class);

    public ClientAddedEvent(Object source) {
        super(source);
    }
}
