function validateClientAdd(){
var name = $("#name").val();
var city = $("#city").val();
var email = $("#email").val();
var balance = $("#balance").val();
var result = true;
var errors = "";

if (name.length < 1){
    errors += "<tr><td>ФИО не заполнено<td></tr>"
    result = false;
}
if (city.length < 1){
    errors += "<tr><td>city не заполнено<td></tr>"
    result = false;
}
if (email.length < 1){
    errors += "<tr><td>email не заполнено<td></tr>"
    result = false;
} else {
    if (!email.match(".+@.+[.][a-zA-Z]{2,3}")) {
            errors += "<tr><td>email имеет неправильный формат<td></tr>"
            result = false;
    }
}
if (balance.length < 1){
    errors += "<tr><td>balance не заполнено<td></tr>"
    result = false;
}
if (errors != ""){
$("#errors").html(errors);
result = false;
} else {
$("#errors").html("<tr><td>Успех<td></tr>");
}
return result;
}