<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <link rel="stylesheet" href="./resources/css/bankStyles.css">
    <script type="text/javascript" src="./resources/js/clientInfo.js"></script>

    <title>Редактирование клиента</title>
</head>
<body>
<div class="centerHigher">
<form id="form" action="/BankApp/saveClient" method="POST">
<input type="hidden" name="clientId" value="${client.id}">
    <table>
        <tr>
            <td colspan=2 style="font-weight:bold;">
            <% if (Math.random() < 0.5) { %>
                <h3>Неудачного вам дня</h3>
                <% } else { %>
                <h3>Удачного вам дня</h3>
                <% } %>
            </td>
        </tr>
        <tr>
            <td>Полное имя:</td>
            <td><input type="text" name="name" id="name" value="${client.name}" readonly></td>
        </tr>
        <tr>
            <td>Город:</td>
            <td><input type="text" name="city" id="city" value="${client.city}"></td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td><input type="text" name="email" id="email" value="${client.email}"></td>
        </tr>
        <tr>
            <td>Phone:</td>
            <td><input type="text" name="phone" id="phone" class="phone" value="${client.phone}"></td>
        </tr>
        <tr>
            <td>Баланс:</td>
            <td><input type="text" name="balance" id="balance" value="${client.balance}" readonly></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <input type="submit" onclick="return validateClientAdd()" value="Изменить клиента">
            </td>
        </tr>
    </table>
</form>
 <table id="errors" class="error">

    </table>
</div>
</body>
</html>