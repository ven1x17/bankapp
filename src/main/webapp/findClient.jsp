<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <link rel="stylesheet" href="./resources/css/bankStyles.css">

    <title>Поиск клиента</title>
</head>
<body>
<div class="centerHigher">
    <form id="clientSearchForm" method="post" action="/BankApp/findClient">
        <table>
            <tr>
                <td>
                    Поиск клиента
                </td>
            </tr>
            <tr>
                <td>
                    Город:
                    <input type="text" name="city" id="city" value="">
                    Имя:
                    <input type="text" name="name" id="name" value="">
                    <input type="submit" value="Найти">
                </td>
            </tr>
        </table>
    </form>
    <c:if test="${clients != null}">
        <h3 id="searchResult">Результат поиска:</h3>
    </c:if>
    <table id="searchResult">
<c:forEach var="client" items="${clients}">
   <tr>
   <td><a href="/BankApp/client?clientId=${client.id}">Name: <c:out value="${client.name}"/> </a></td>
   <td>City: <c:out value="${client.city}"/></td>
   <td>Balance: <c:out value="${client.balance}"/></td>
   </tr>
</c:forEach>
    </table>
</div>
</body>
</html>